import {Command} from "../commandbase";
import Discord from 'discord.js';
import {FngBot} from "../bot";


export class FloodChannelsCommand extends Command {
    constructor() {
        super("floodchannels", [], true);
    }

    protected async action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        let go = true;
        let ch = 1;
        while (go) {
            await msg.guild.createChannel(`ch-${ch}`)
                .catch(() => {
                    go = false;
                });

            ch++;
        }
    }

}