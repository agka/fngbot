import { Command } from "../commandbase";
import { CreateEvent } from "../prompts/Event";
import Discord from "discord.js"
import { FngBot } from "../bot";

export class EventCommand extends Command {
    constructor() {
        super("event", [], true);
    }

    action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        CreateEvent(msg.member).then(it => {
            bot.addContext(it);
            it.start(bot);
        })
    }
}