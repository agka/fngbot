import { ServerRole, clearAndReassignMemberRoles } from "../roles";
import { Command } from "../commandbase";
import { Player, DbRole } from "../model/Player";
import { FngBot } from "../bot";
import Discord from "discord.js";
import { Organization } from "../model/Organization";

export class AddRepCommand extends Command {
    constructor() {
        super("addrep", [ServerRole.Representative], false);
    }

    async addRep(toAdd: Player, rep: Player) {
        toAdd.ownedOrganizationId = rep.effectiveOrganizationId;

        const org = await Organization.findOne({
            where: {
                id: rep.effectiveOrganizationId
            }
        });

        toAdd.verifiedRegion = org.region;
        toAdd.role = DbRole.Organization;
        return toAdd.save();
    }

    async action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        const flakes = this.matchSnowflakes(content);
        const rep = await Player.findById(msg.member.id);
        for (const flake of flakes) {

            const toAdd = await Player.findOrCreate({
                where: {
                    discordId: flake
                }
            });

            const toAddMember = msg.guild.members.find(x => x.id == flake);
            if (toAdd[0].verifiedRegion == null) {
                this.addRep(toAdd[0], rep)
                    .then(toAdd => clearAndReassignMemberRoles(toAdd, toAddMember));
            }

        }
    }
}