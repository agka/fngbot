import { ServerRole, clearAndReassignMemberRoles, hasServerRole } from "../roles";
import { Command } from "../commandbase";
import { Player, DbRole } from "../model/Player";
import Discord from "discord.js"
import { FngBot } from "../bot";

export class OwnerCommand extends Command {
    constructor() {
        super("owner", [ServerRole.Captain], false);
    }

    async action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        const flakes = this.matchSnowflakes(content);

        if (flakes.length === 0) {
            msg.reply("Mention who you want to transfer ownership to as part of the command.");
            return;
        }

        const flake = flakes[0];
        const toOwnerMember = msg.guild.members.find(x => x.id === flake);

        if (!toOwnerMember) {
            msg.reply("You can only transfer ownership to someone that is on the server.");
            return;
        }

        const toOwnerPlayer = await Player.find({
            where: {
                discordId: flake
            }
        });

        if (!toOwnerPlayer || toOwnerPlayer.verifiedRegion == null) {
            msg.reply("The user you mentioned is not verified.");
            return;
        }

        const captain = await Player.find({
            where: {
                discordId: msg.member.id
            }
        });

        if (captain.effectiveTeamId !== toOwnerPlayer.effectiveTeamId) {
            msg.reply("You can only turn into an owner someone who is already on your team.");
            return;
        }

        const teamId = captain.effectiveTeamId;

        // update captain -> player
        await Player.update({
            teamId: teamId,
            ownedTeamId: null,
            role: DbRole.Player
        }, {
                where: {
                    discordId: captain.discordId
                }
            });

        // reset here for checkMemberRole purposes
        captain.teamId = teamId;
        captain.ownedTeamId = null;
        captain.role = DbRole.Player;
        await clearAndReassignMemberRoles(captain, msg.member);

        // update player -> captain
        await Player.update({
            teamId: null,
            ownedTeamId: teamId,
            role: DbRole.Team
        }, {
                where: {
                    discordId: flake
                }
            });

        toOwnerPlayer.teamId = null;
        toOwnerPlayer.ownedTeamId = teamId;
        toOwnerPlayer.role = DbRole.Team;
        await clearAndReassignMemberRoles(toOwnerPlayer, toOwnerMember);
    }
}