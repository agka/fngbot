import { Command } from "../commandbase";
import { Event, EventStatus } from "../model/Event";
import Discord from "discord.js"
import { FngBot } from "../bot";

export class UpdateCommand extends Command {
    constructor() {
        super("update", [], true);
    }

    action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        const id = parseFloat(content);

        if (isNaN(id)) {
            msg.reply("Not a valid event ID. Try a number.");
        }

        Event.findById(id).then(evt => {
            if (evt === null) {
                msg.reply(`Event ${id} not found.`);
                return;
            }


            switch (evt.status) {
                case EventStatus.Finished:
                    msg.reply(`Event '${evt.name}' is already finished.`);
                    return; // no-op
                case EventStatus.Future:
                    msg.reply(`Event '${evt.name}' is now set to ongoing.`);
                    evt.status = EventStatus.Ongoing;
                    break;
                case EventStatus.Ongoing:
                    msg.reply(`Event '${evt.name}' is now set to finished.`);
                    evt.status = EventStatus.Finished;
                    break;
            }

            bot.repostEventMessage(evt);
        });
    }
}