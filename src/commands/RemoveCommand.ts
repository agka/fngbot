import { ServerRole, clearAndReassignMemberRoles, hasServerRole } from "../roles";
import Discord from "discord.js";
import { Player, DbRole } from "../model/Player";
import { Command } from "../commandbase";
import { FngBot } from "../bot";

export class RemoveCommand extends Command {
    constructor() {
        super("remove", [ServerRole.Representative, ServerRole.Captain], false);
    }

    async removeFromOrg(player: Player, representative: Player, msg: Discord.Message) {
        // updating differently causes col to not be updated
        await Player.update({ organizationId: null }, {
            where: {
                discordId: player.discordId
            }
        });

        // udpate this because of member role check
        player.organizationId = null;

        const member = msg.guild.members.find(x => x.id === player.discordId);
        if (member)
            clearAndReassignMemberRoles(player, member);
    }

    async removeFromTeam(player: Player, captain: Player, msg: Discord.Message) {
        // updating differently causes col to not be updated
        await Player.update({ teamId: null }, {
            where: {
                discordId: player.discordId
            }
        });

        // needs this update because of the member role check
        player.teamId = null;

        const member = msg.guild.members.find(x => x.id === player.discordId);
        if (member)
            clearAndReassignMemberRoles(player, member);
    }

    remove(flake: string, msg: Discord.Message) {
        Player.find({
            where: {
                discordId: flake
            }
        }).then(async player => {
            if (!player) return;


            if (hasServerRole(msg.member, ServerRole.Representative)) {
                const representative = await Player.findById(msg.member.id);

                // non-representatives
                if (player.organizationId === representative.ownedOrganizationId)
                    this.removeFromOrg(player, representative, msg);
                else {
                    if (player.ownedOrganizationId === representative.ownedOrganizationId) {
                        msg.reply("You can't remove another representative.");
                    }

                    if (player.organizationId !== representative.ownedOrganizationId) {
                        msg.reply("You can only remove people in your organization.");
                    }
                }
            }

            if (hasServerRole(msg.member, ServerRole.Captain)) {
                const captain = await Player.findById(msg.member.id);

                // not-captain
                if (player.teamId === captain.ownedTeamId)
                    this.removeFromTeam(player, captain, msg);
                else {
                    msg.reply("You can only remove people that are already on your team.");
                }
            }
        })
    }

    action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        // console.log(content);
        const flakes = this.matchSnowflakes(content);
        for (var flake of flakes) {
            // console.log(content);
            this.remove(flake, msg);
        }
    }
}
