import { Command } from "../commandbase";
import Discord from "discord.js"
import { FngBot } from "../bot";

export class ResumeCommand extends Command {
    constructor() {
        super("resume", [], true);
    }

    action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        if (bot.allowApplications)
            msg.reply("Applications are already open.");
        else
            msg.reply("Applications are now open.");

        bot.allowApplications = true;
    }
}