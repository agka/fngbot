import { Command } from "../commandbase";
import Discord from "discord.js"
import { FngBot } from "../bot";

export class PauseCommand extends Command {
    constructor() {
        super("pause", [], true);
    }

    action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        if (bot.allowApplications)
            msg.reply("Applications are now closed temporarily.");
        else
            msg.reply("Applications are already closed at the time.");

        bot.allowApplications = false;
    }
}