import { ServerRole, clearAndReassignMemberRoles, hasServerRole } from "../roles";
import Discord from "discord.js";
import { Player, DbRole } from "../model/Player";
import { Command } from "../commandbase";
import { Sequelize } from "sequelize-typescript";
import { FngBot } from "../bot";
import { Team } from "../model/Team";
import { Organization } from "../model/Organization";

export class AddCommand extends Command {
    constructor() {
        super("add", [ServerRole.Representative, ServerRole.Captain], false);
    }

    addToTeam(
        playerToPutInTeam: Player, 
        captain: Discord.GuildMember,
        msg: Discord.Message) {
        Player.findById(captain.id).then(async capt => {

            if (!capt) {
                console.warn(`${captain.id} has captain role, but isn't on the database.`);
                return;
            }

            const plist = await Player.findAll({
                where: {
                    [Sequelize.Op.or]: [
                        {teamId: capt.effectiveTeamId},
                        {ownedTeamId: capt.effectiveTeamId}
                    ]
                }
            });

            if (plist.length >= 10) {
                msg.reply("You've already reached the cap of 10 players on a team.");
                return; // can't add anyone else
            }

            playerToPutInTeam.teamId = capt.effectiveTeamId;
            if (playerToPutInTeam.verifiedRegion == null) {
                const team = await Team.findOne({
                    where: {
                        id: capt.effectiveTeamId
                    }
                });
                
                playerToPutInTeam.verifiedRegion = team.region;
                playerToPutInTeam.role = DbRole.Player;
            }

            await playerToPutInTeam.save();

            const member = captain.guild.members.find(x => x.id == playerToPutInTeam.discordId);
            if (member)
                await clearAndReassignMemberRoles(playerToPutInTeam, member);
        });
    }

    addToOrg(playerToPutInOrg: Player, representative: Discord.GuildMember) {
        Player.findById(representative.id).then(async repre => {
            if (!repre) {
                console.warn(`${representative.id} has representative role, but isn't on the database.`);
                return;
            }

            playerToPutInOrg.organizationId = repre.effectiveOrganizationId;
            if (playerToPutInOrg.verifiedRegion == null) {
                const org = await Organization.findOne({
                    where: {
                        id: repre.effectiveOrganizationId
                    }
                });

                playerToPutInOrg.verifiedRegion = org.region;
                playerToPutInOrg.role = DbRole.Player;
            }

            await playerToPutInOrg.save();

            const member = representative.guild.members.find(x => x.id == playerToPutInOrg.discordId);

            if (member)
                await clearAndReassignMemberRoles(playerToPutInOrg, member);
        });
    }

    private add(flake: string, msg: Discord.Message) {
        Player.findOrCreate({
            where: {
                discordId: flake
            },
            defaults: {
                discordId: flake,
                role: DbRole.Player
            }
        }).then(arr => {
            const player = arr[0];
            const added = arr[1];
            
            // sender is representative
            if (hasServerRole(msg.member, ServerRole.Representative)) {
                if (player.hasOrganization) {
                    msg.reply(`${player.discordId} already is on an organization.`);
                }
                else {
                    this.addToOrg(player, msg.member);
                }
            }

            // sender is team captain
            if (hasServerRole(msg.member, ServerRole.Captain)) {
                if (player.hasTeam) {
                    msg.reply(`${player.discordId} already is on a team.`);
                }
                else {
                    this.addToTeam(player, msg.member, msg);
                }
            }
        });
    }
    
    action(
        content: string, 
        msg: Discord.Message, 
        bot: FngBot,
        isStaffCaller: boolean) {
        const flakes = this.matchSnowflakes(content);
        for (const flake of flakes) {
            const mem = msg.guild.members.find(x => x.id === flake);
            const isStaffOrAdmin = mem && (hasServerRole(mem, ServerRole.Admin) || 
                                   hasServerRole(mem, ServerRole.Staff));
            
            // member not in server or, not staff or admin
            if (!isStaffOrAdmin)
                this.add(flake, msg);
            else {
                msg.reply("You can't add someone from the administration or staff to your team/organization.");
            }
        }
    }
}