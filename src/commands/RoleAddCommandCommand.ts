import { Command } from "../commandbase";
import { CommandPermission } from "../model/CommandPermissions";
import { commandExists } from "../commandList";
import Discord from "discord.js"
import { FngBot } from "../bot";

export class RoleAddCommandCommand extends Command {
    constructor() {
        super("roleadd", [], true);
    }

    action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        const split = content.split(" ")

        if (split.length < 2) {
            msg.reply("Please specify the command and the role, in that order.");
            return;
        }

        const cmd = split[0];
        const flakes = this.matchSnowflakes(split.slice(1).join(" "));

        for (const flake of flakes) {
            const role = msg.guild.roles.find(x => x.id === flake);

            if (!role) {
                msg.reply(`The role snowflake '${flake}' doesn't exist.`);
                continue;
            }

            if (!commandExists(cmd)) {
                msg.reply(`The command ${cmd} doesn't exist.`);
                continue;
            }

            CommandPermission.findOrCreate({
                where: {
                    command: cmd,
                    roleSnowflake: flake
                }
            }).then(res => {
                const perm = res[0];
                const inserted = res[1];

                if (inserted) {
                    msg.reply(`Added permission to '${cmd}' to ${role.name}`);
                } else {
                    msg.reply(`${role.name} already can '${cmd}.`);
                }
            })
        }
    }
}