import { ServerRole } from "../roles";
import Discord from "discord.js";
import { Player } from "../model/Player";
import { Command } from "../commandbase";
import { FngBot } from "../bot";

export class ProfileCommand extends Command {
    constructor() {
        super("profile", [ServerRole.Verified], false);
    }

    action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        Player.findById(msg.member.id)
            .then(player => {
                if (player)
                    bot.promptProfileCreation(player, msg.member);
            });
    }
}