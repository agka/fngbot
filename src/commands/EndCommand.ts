import { Command } from "../commandbase";
import Discord from "discord.js"
import { FngBot } from "../bot";
import { endPlayer } from "../disbandAndEnd";
import { Player } from "../model/Player";
import { ServerRole } from "../roles";

export class EndCommand extends Command {
    constructor() {
        super("end", [ServerRole.Verified], true);
    }
    
    protected action(
        content: string, 
        msg: Discord.Message, 
        bot: FngBot, 
        isStaffCaller: boolean): void {
        if (isStaffCaller)
            this.staffEnd(content, msg, bot);
        else 
            this.endSelf(content, msg, bot);
    }

    async endSelf(content: string, msg: Discord.Message, bot: FngBot) {
        const player = await Player.findOne({
            where: {
                discordId: msg.member.id
            }
        });

        if (!player) {
            console.error(`Player not found when trying to -end: ${msg.member.id}`);
            return;
        }

        await endPlayer(player, msg.guild);
        msg.reply("You've de-registered from the system succesfully.");
    }

    async staffEnd(content: string, msg: Discord.Message, bot: FngBot) {
        const flakes = this.matchSnowflakes(content);

        for (var flake of flakes) {
            const player = await Player.findOne({
                where: {
                    discordId: flake
                }
            });

            await endPlayer(player, msg.guild);
            await msg.reply(`${flake} has been removed from the database.`);
        }
    }
    
    
}