import { ServerRole, dbRoleFromCharacter } from "../roles";
import { Command } from "../commandbase";
import { Apply } from "../prompts/Apply";
import { UserApplication } from "../model/UserApplication";
import { Sequelize } from "sequelize-typescript";
import { FngBot } from "../bot";
import Discord from "discord.js"

export class ApplyCommand extends Command {

    constructor() {
        super("apply", [ServerRole.Upcoming], false);
    }

    protected async action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean): Promise<void> {
        content = content.toLowerCase();

        if (!bot.allowApplications) {
            msg.reply("Applications are not allowed at this time. Try again later.");
            return;
        }

        const app = await UserApplication.findOne({
            where: {
                playerId: msg.member.id,
                accepted: {
                    [Sequelize.Op.eq]: null
                }
            }
        });

        if (app != null) {
            msg.reply(`You already have a pending application.`);
            return;
        }

        const specify = "Please specify if you want to apply for team captain, organization or player by writing T, O or P.";
        if (!content.length) {
            msg.reply(specify);
            return;
        }

        const role = dbRoleFromCharacter(content[0]);
        if (role === null) {
            msg.reply("That's not a valid application type, sorry. Try T, O or P.");
            return;
        }

        // console.log(role);

        Apply(msg.member, role).then(it => {
            bot.addContext(it);
            it.start(bot);
        });
    }

}