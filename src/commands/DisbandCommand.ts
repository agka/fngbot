import { Command } from "../commandbase";
import { Team } from "../model/Team";
import { Organization } from "../model/Organization";
import { disbandTeam, disbandOrganization } from "../disbandAndEnd";
import Discord from "discord.js"
import { FngBot } from "../bot";
import { hasServerRole, ServerRole } from "../roles";
import { Player } from "../model/Player";

export class DisbandCommand extends Command {
    constructor() {
        super("disband", [ServerRole.Representative, ServerRole.Captain], true);
    }

    async disbandForStaff(content: string, msg: Discord.Message, bot: FngBot) {
        const split = content.split(" ")

        if (split.length < 2) {
            msg.reply("Please specify if you're disbanding a team or org (T, O), and its ID.");
            return;
        }

        const toDisband = split[0].trim().charAt(0);
        const id = parseFloat(split[1]);

        if (isNaN(id)) {
            msg.reply("");
            return;
        }

        switch (toDisband) {
            case "T":
                const team = await Team.findById(id);

                if (team)
                    disbandTeam(team, msg.guild);
                else
                    msg.reply("The team doesn't exist.");
                break;
            case "O":
                const org = await Organization.findById(id);
                if (org)
                    disbandOrganization(org, msg.guild);
                else
                    msg.reply("The organization doesn't exist.");
                break;
            default:
                msg.reply("You can only disband a (T)eam or an (O)rganization.");
                return;
        }
    }

    async disbandForGeneral(content: string, msg: Discord.Message, bot: FngBot) {
        const player = await Player.findOne({
            where: {
                discordId: msg.member.id
            }
        });

        if (!player) {
            console.log(`User ${msg.member.id} that isn't verified tried to disband`);
            return; // ???
        }


        if (hasServerRole(msg.member, ServerRole.Captain)) {
            const team = await Team.findById(player.effectiveTeamId);

            await disbandTeam(team, msg.guild);
            msg.reply(`Your team has been disbanded.`);
        }

        if (hasServerRole(msg.member, ServerRole.Representative)) {
            const org = await Organization.findById(player.effectiveOrganizationId);

            await disbandOrganization(org, msg.guild);
            msg.reply(`Your organization has been disbanded.`);
        }
    } 

    async action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        if (isStaffCaller)
            this.disbandForStaff(content, msg, bot);
        else
            this.disbandForGeneral(content, msg, bot);
    }
}