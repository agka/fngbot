import { Command } from "../commandbase";
import { ServerRole, clearAndReassignMemberRoles } from "../roles";
import { Team } from "../model/Team";
import { Player } from "../model/Player";
import Discord from "discord.js";
import { FngBot } from "../bot";
import { Sequelize } from "sequelize-typescript";

export class AddTeamCommand extends Command {
    constructor() {
        super("addteam", [ServerRole.Representative], false);
    }

    async action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        const id = parseFloat(content);
        const team = await Team.findById(id);
        if (team.organizationId) {
            msg.reply(`${team.name} already is part of an organization.`);
            return;
        }

        const player = await Player.find({
            where: {
                discordId: msg.member.id
            }
        });

        team.organizationId = player.effectiveOrganizationId;
        team.save();

        msg.reply(`Team ${team.name} has been added to your organization.`);

        // reassign roles for players on this team
        const playerList = await Player.findAll({
            where: {
                [Sequelize.Op.or]: {
                    teamId: team.id,
                    ownedTeamId: team.id
                }
            }
        });

        for (const player of playerList) {
            const member = msg.guild.members.find(x => x.id == player.discordId);
            if (member) {
                await clearAndReassignMemberRoles(player, member);
            } else {
                console.log(`[addteam] couldn't find ${player.discordId} on the guild to give them roles`);
            }
        }
    }
}