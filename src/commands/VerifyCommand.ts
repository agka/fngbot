import { clearAndReassignMemberRoles, dbRoleFromCharacter } from "../roles";
import { Player, Region } from "../model/Player";
import { Command } from "../commandbase";
import Discord from "discord.js";
import { FngBot } from "../bot";

function strToRegionTag(str: string) {
    if (!str)
        return null;

    switch(str.toLowerCase()) {
        case "na":
            return Region.NA;
        case "eu":
            return Region.EU;
        case "sea":
            return Region.SEA;
        case "oce":
            return Region.OCE;
        default: 
            return null;
    }
}

export class VerifyCommand extends Command {

    constructor() {
        super('verify', [], true);
    }

    action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        content = content.trim();

        const split = content.split(" ");

        if (split.length < 3) {
            msg.reply("Please write region tag, `T`,`O`, or `P`" + 
                " and put snowflake or mention someone.");
            return;
        }

        const flakes = this.matchSnowflakes(content);

        const region = strToRegionTag(split[0]);
        const type = split[1];
        const flake = flakes[0];

        if (region === null) {
            msg.reply("Invalid region '" + region + "', try one of: `na`, `sea`, `eu` or `oce`.");
            return;
        }

        if (!flake) {
            msg.reply("Couldn't match a user snowflake or mention to verify.");
            return;
        }

        Player.findOrCreate({
            where: {
                discordId: flake
            },
            defaults: {
                discordId: flake,
            }
        }).then(async result => {
            const player = result[0];
            const added = result[1];
            const role = dbRoleFromCharacter(type);

            // disallow reverifying
            if (player.verifiedRegion != null) {
                msg.reply("User is already verified.");
                return;
            }

            // console.log(role);
            if (role !== null) {
                // console.log("verifying");
                player.verifiedRegion = region;
                player.role = role;
                await player.save();

                const member = msg.guild.members.find(x => x.id === player.discordId);
                // console.log(player.discordId);
                // console.log(member);
                if (member)
                    clearAndReassignMemberRoles(player, member);
            } else {
                msg.reply("Invalid role.");
            }
        });

    }
}