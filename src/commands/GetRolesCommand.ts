import { Command } from "../commandbase";
import Discord from "discord.js";
import { FngBot } from '../bot'

export class GetRolesCommand extends Command {
    protected action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean): void {
        const guild = msg.guild;
        const lines: string[] = [];

        for (const role of guild.roles.array()) {
            lines.push(`${role.id}:${role.name} pos/cpos: ${role.position}/${role.calculatedPosition}`);
        }

        msg.reply(lines.join("\n"));
    }
    constructor() {
        super("debugroles", [], true);
    }

}