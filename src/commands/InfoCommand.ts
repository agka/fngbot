import {Command} from "../commandbase";
import {FngBot} from "../bot";
import Discord from 'discord.js';
import {dbRoleFromCharacter, ServerRole} from "../roles";
import {DbRole, Player} from "../model/Player";
import {createOrgEmbed, createPlayerEmbed, createTeamEmbed} from "../profiles";
import {Organization} from "../model/Organization";
import {Team} from "../model/Team";


export class InfoCommand extends Command {
    constructor() {
        super("info", [ServerRole.Verified], true);
    }

    protected async action(content: string, msg: Discord.Message, bot: FngBot, isStaffCaller: boolean) {
        const splitcontents = content.split(" ");

        const ch = splitcontents[0];
        const role = dbRoleFromCharacter(ch);

        if (role == null) {
            msg.reply("Unknown role. Try T, O or P.");
            return;
        }

        const ref = content.substr(content.indexOf(" ") + 1);

        switch (role) {
            case DbRole.Team:
                const teamName = ref;
                const team = await Team.findOne({
                    where: {
                        name: teamName
                    }
                });

                if (team == null) {
                    msg.reply("Team not found.");
                    return;
                }

                const tembed = await createTeamEmbed(team, msg.guild);
                msg.reply(tembed);
                break;
            case DbRole.Player:
                const flake = this.matchSnowflakes(splitcontents[1]);
                const player = await Player.findOne({
                    where: {
                        discordId: flake
                    }
                });

                if (player == null) {
                    msg.reply("Player not found.");
                    return;
                }

                if (!player.hasPlayerProfile) {
                    msg.reply("This user doesn't have a profile.");
                    return;
                }

                const pembed = await createPlayerEmbed(player, msg.guild);
                msg.reply(pembed);
                break;
            case DbRole.Organization:
                const orgName = ref;
                const org = await Organization.findOne({
                    where: {
                        name: orgName
                    }
                });

                if (org == null) {
                    msg.reply("Organization not found.");
                    return;
                }

                const oembed = await createOrgEmbed(org/*, msg.guild*/);
                msg.reply(oembed);
                break;
        }
    }

}