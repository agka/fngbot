import Discord from "discord.js";
import { Region } from "./model/Player";
const config = require("../config.json");

export enum PromptAction {
    Repeat, // rerun this current state
    Advance, // advance to the next state
    Nothing // neither rerun nor advance - do nothing
}

export type Prompt = {
    id: number,
    action?: () => void,
    getMessage?: () => string,
    reactions?: string[],
    onReact?: (reaction: Discord.MessageReaction) => PromptAction,
    onResponse?: (msg: string) => PromptAction,
    getNextPrompt: () => number,
    accept?: boolean,
    embed?: () => Promise<Discord.RichEmbed>
}

export type PromptTransitionList = {
    [id: number]: Prompt
};

export function addEmbed(prompt: Prompt, embed: () => Promise<Discord.RichEmbed>) {
    prompt.embed = embed;
    return prompt;
}

export function condition(
    id: number, 
    condition: () => boolean, 
    nextIfTrue: number, 
    nextIfFalse: number) : Prompt {
    return {
        id: id,
        getNextPrompt: () => condition() ? nextIfTrue : nextIfFalse
    };
}

// finish the registration process and delete the channel
export function accept(id: number) : Prompt {
    return {
        id: id,
        getNextPrompt: () => -1,
        accept: true
    }
}

export function action(id: number, action: () => void, next: number) : Prompt
{
    return {
        id: id,
        action: action,
        getNextPrompt: () => next
    }
}

// prompt for a string
export function stringPrompt(
    id: number, 
    question: () => string, 
    resultCallback: (r: string) => void, 
    next: number) : Prompt {
    return {
        id: id,
        getMessage: question,
        onResponse: (msg: string) => {
            resultCallback(msg)
            return PromptAction.Advance;
        },
        getNextPrompt: () => next
    };
}

// prompt for a number
export function numberPrompt(
    id: number, 
    question: () => string, 
    resultCallback: (r: number) => void, 
    next: number, 
    range?: [number, number]) : Prompt {
    var sayItWasInvalid = false;
    return {
        id: id,
        getMessage: () => {
            if (sayItWasInvalid)
                return "Not a valid number or out of range.\r\n" + question();
            else 
                return question();
        },
        onResponse: (msg: string) => {
            if (isNaN(parseInt(msg)))
            {
                sayItWasInvalid = true;
                return PromptAction.Repeat;
            }
            else {
                const num = parseInt(msg);
                
                if (range)
                    if (num < range[0] || num > range[1])
                    {
                        sayItWasInvalid = true;
                        return PromptAction.Repeat;
                    }

                resultCallback(parseInt(msg));
                return PromptAction.Advance;
            }
        },
        getNextPrompt: () => next
    }
}

export function reactOneTwoThreeOrNone(
    id: number,
    question: () => string,
    resultCallback: (count: number) => PromptAction,
    next: number
) : Prompt{
    
    let reactions = [
        config.reactions.zero, 
        config.reactions.one, 
        config.reactions.two, 
        config.reactions.three
    ];

    return {
        id: id,
        getMessage: question,
        onReact: mr => {
            switch(mr.emoji.name) {
                case reactions[0]:
                    resultCallback(0);
                    break;
                case reactions[1]:
                    resultCallback(1);
                    break;
                case reactions[2]:
                    resultCallback(2);
                    break;
                case reactions[3]:
                    resultCallback(3);
                    break;
                default:
                    return PromptAction.Nothing;
            }

            return PromptAction.Advance;
        },
        reactions: reactions,
        getNextPrompt: () => next
    };
}

export function reactRegion(
    id: number,
    question: () => string,
    resultCallback: (region: number) => void,
    next: number
) : Prompt {
    const reactions = [
        config.reactions.na, 
        config.reactions.eu,
        config.reactions.sea,
        config.reactions.oce
    ]

    return {
        id: id,
        getMessage: question,
        onReact: mr => {
            switch(mr.emoji.name) {
                case reactions[0]:
                    resultCallback(Region.NA);
                    break;
                case reactions[1]:
                    resultCallback(Region.EU);
                    break;
                case reactions[2]:
                    resultCallback(Region.SEA);
                    break;
                case reactions[3]:
                    resultCallback(Region.OCE);
                    break;
                default:
                    return PromptAction.Nothing;
            }

            return PromptAction.Advance;
        },
        reactions: reactions,
        getNextPrompt: () => next
    }
}

export function reactYesNo(
    id: number,
    question: () => string,
    resultCallback: (yes: boolean) => void,
    next: number
) : Prompt {
    return {
        id: id,
        getMessage: question,
        onReact: mr => {
            switch (mr.emoji.name) {
                case config.reactions.ok:
                    resultCallback(true);
                    return PromptAction.Advance;
                case config.reactions.cancel:
                    resultCallback(false);
                    return PromptAction.Advance;
            }

            return PromptAction.Nothing;
        },
        reactions: [config.reactions.ok, config.reactions.cancel],
        getNextPrompt: () => next
    }
}

export function reactYesNoBranch(
    id: number,
    question: () => string,
    nextIfTrue: number,
    nextIfFalse: number
) : Prompt {
    let result = false;
    return {
        id: id,
        getMessage: question,
        onReact: mr => {
            switch (mr.emoji.name) {
                case config.reactions.ok:
                    result = true;
                    return PromptAction.Advance;
                case config.reactions.cancel:
                    result = false;
                    return PromptAction.Advance;
            }

            return PromptAction.Nothing;
        },
        reactions: [config.reactions.ok, config.reactions.cancel],
        getNextPrompt: () => result ? nextIfTrue : nextIfFalse
    }
}

export function gatherStrings(
    id: number,
    question: () => string,
    resultCallback: (res: string) => PromptAction,
    next: number
) : Prompt {
    return {
        id: id,
        getMessage: question, 
        onResponse: resultCallback,
        getNextPrompt: () => next
    }
}