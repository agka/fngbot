import {UserApplication} from "./model/UserApplication";
import {DbRole, Player} from "./model/Player";
import Discord from 'discord.js';
import {ApplicationPoint} from "./model/ApplicationPoint";
import {PlayerAchievement} from "./model/PlayerAchievement";
import {Team} from "./model/Team";
import {TeamAchievements} from "./model/TeamAchievements";
import {Organization} from "./model/Organization";
import {KeyPoints} from "./model/KeyPoints";
import {clearAndReassignMemberRoles, makeOrganizationRole} from "./roles";

const config = require("../config.json");

function getApplicationPoints(app: UserApplication) {
    return ApplicationPoint.findAll({
        where: {
            applicationId: app.id
        }
    });
}

function updatePlayerRoles(guild: Discord.Guild, player: Player) {
    const member = guild.members.find(x => x.id == player.discordId);
    if (!member) {
        console.log(`member not found when approving application: ${player.discordId}`);
        return;
    }

    return clearAndReassignMemberRoles(player, member);
}

async function moveApplicationToTeam(app: UserApplication, guild: Discord.Guild) {
    const appPoints = await getApplicationPoints(app);

    const team = await Team.create({
        name: app.name,
        description: app.description,
        website: app.website,
        region: app.region,
        organizationId: null
    });

    const player = await Player.create({
        discordId: app.playerId,
        verifiedRegion: app.region,
        role: DbRole.Team,
        ownedTeamId: team.id
    });

    for (const point of appPoints) {
        await TeamAchievements.create({
            teamId: team.id,
            description: point.description
        });
    }

    updatePlayerRoles(guild, player);
}

async function moveApplicationToPlayer(app: UserApplication, guild: Discord.Guild) {
    const appPoints = await getApplicationPoints(app);

    const player = await Player.create({
        discordId: app.playerId,
        ign: app.name,
        bio: app.description,
        verifiedRegion: app.region,
        role: DbRole.Player,
        socialMedia: app.website
    });

    for (const point of appPoints) {
        await PlayerAchievement.create({
            playerId: app.playerId,
            description: point.description
        });
    }

    updatePlayerRoles(guild, player);
}

async function moveApplicationToOrganization(app: UserApplication, guild: Discord.Guild) {
    const appPoints = await getApplicationPoints(app);

    const org = await Organization.create({
        name: app.name,
        description: app.description,
        region: app.region,
        website: app.website,
        email: app.email
    });

    await makeOrganizationRole(guild, org);

    const player = await Player.create({
        discordId: app.playerId,
        verifiedRegion: app.region,
        role: DbRole.Organization,
        ownedOrganizationId: org.id
    });

    for (let point of appPoints) {
        await KeyPoints.create({
            organizationId: org.id,
            description: point.description
        });
    }

    updatePlayerRoles(guild, player);
}

export function approveApplication(application: UserApplication, guild: Discord.Guild) {

    switch (application.applicationType) {
        case DbRole.Team:
            moveApplicationToTeam(application, guild);
            break;
        case DbRole.Player:
            moveApplicationToPlayer(application, guild);
            break;
        case DbRole.Organization:
            moveApplicationToOrganization(application, guild);
            break;
        default:
            console.info(`Tried to approve application ${application.id} for unknown role ${application.applicationType}`);
    }

    application.accepted = true;
    application.save();
}

export function rejectApplication(application: UserApplication) {
    application.accepted = false;
    return application.save();
}


function applicationReactionFilter(reac: Discord.MessageReaction, mem: Discord.ClientUser): boolean {
    return !mem.bot;
}

export async function addMessageFunction(application: UserApplication, msg: Discord.Message) {
    const okEmoji = msg.guild.emojis.find(x => x.name === config.reactions.ok);
    const cancelEmoji = msg.guild.emojis.find(x => x.name === config.reactions.cancel);

    await msg.react(okEmoji).catch(() => {
    });
    await msg.react(cancelEmoji).catch(() => {
    });


    const coll = msg.createReactionCollector(applicationReactionFilter, {
        max: 10,
        time: 207360 // 2.4 days (ought to be enough?)
    });

    var approved = false;

    coll.on("collect", (el) => {
        // console.log(el.emoji);

        // console.log(okEmoji);

        if (approved) return;

        approved = true;
        if (el.emoji.name == okEmoji.name) {
            approveApplication(application, msg.guild);
        }

        if (el.emoji.name == cancelEmoji.name) {
            rejectApplication(application);
        }
    });
}

