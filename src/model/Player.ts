import {PlayerAchievement} from './PlayerAchievement';

import {Organization} from './Organization';

import {Table, Column, Model, HasMany, Unique, BelongsTo, ForeignKey} from 'sequelize-typescript';
import {Team} from './Team';

export enum DbRole {
    Player,
    Team,
    Organization
}

export enum Region {
    NA,
    EU,
    SEA,
    OCE
}

@Table
export class Player extends Model<Player> {

    @Unique
    @Column({primaryKey: true})
    discordId: string;

    @Column
    ign: string;

    @Column
    bio: string;

    @Column
    verifiedRegion: number;

    @Column
    role: number;

    @HasMany(() => PlayerAchievement)
    achievement?: PlayerAchievement[];

    @Column
    socialMedia: string;

    // Organization
    // several representatives per team
    @ForeignKey(() => Organization)
    @Column
    ownedOrganizationId: number;

    @BelongsTo(() => Organization, "ownedOrganizationId")
    ownedOrganization: Organization;

    @ForeignKey(() => Organization)
    @Column
    organizationId: number;

    @BelongsTo(() => Organization, "organizationId")
    organization: Organization;

    // Teams
    @ForeignKey(() => Team)
    @Unique // only one capt per team
    @Column
    ownedTeamId: number;

    @BelongsTo(() => Team, "ownedTeamId")
    ownedTeam: Team;

    @ForeignKey(() => Team)
    @Column
    teamId: number;

    @BelongsTo(() => Team, "teamId")
    team: Team;

    get hasOrganization(): boolean {
        return (this.ownedOrganizationId || this.organizationId) != null;
    }

    get hasTeam(): boolean {
        return (this.ownedTeamId || this.teamId) != null;
    }

    get effectiveTeamId(): number {
        return this.ownedTeamId ||
            this.teamId;
    }

    get effectiveOrganizationId(): number {
        return this.ownedOrganizationId ||
            this.organizationId;
    }

    get hasPlayerProfile(): boolean {
        return this.ign != null && this.ign != "";
    }

    async teamHasOrganization(): Promise<boolean> {
        const team = await Team.findOne({
            where: {
                id: this.effectiveTeamId
            }
        });

        // console.log(team);

        if (team)
            return team.organizationId != null;

        return false;
    }
}

