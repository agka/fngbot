import { Column, Table, Model, PrimaryKey } from "sequelize-typescript";

// if it exists, it can be used
@Table
export class CommandPermission extends Model<CommandPermission> {

    @PrimaryKey
    @Column 
    command: string;

    @PrimaryKey
    @Column 
    roleSnowflake: string;
}