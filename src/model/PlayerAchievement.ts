import { Table, Column, Model, BelongsTo, NotNull, ForeignKey } from 'sequelize-typescript';
import { Player } from './Player';
@Table
export class PlayerAchievement extends Model<PlayerAchievement> {
    @Column
    description: string;

    @ForeignKey(() => Player)
    @Column
    playerId: string;

    @BelongsTo(() => Player)
    player: Player;
}