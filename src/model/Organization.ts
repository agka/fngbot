import { Table, Column, Model, HasMany, Unique, Sequelize } from 'sequelize-typescript';
import { Player } from './Player';
import { KeyPoints } from './KeyPoints';
import { Team } from './Team';

@Table
export class Organization extends Model<Organization> {
    @HasMany(() => Player, "ownedOrganizationId")
    owners: Player[];

    @HasMany(() => Player, "organizationId")
    players: Player[];

    @HasMany(() => Team, "organizationId")
    teams: Team[];

    @Column
    name: string;

    @Column(Sequelize.STRING(500))
    description: string;

    @Column 
    region: number;

    @Column
    website: string;

    @Column
    email: string;

    @Unique
    @Column
    roleSnowflake: string;


    @HasMany(() => KeyPoints)
    keyPoints: KeyPoints;
}