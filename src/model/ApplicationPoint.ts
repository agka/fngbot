import { ForeignKey, Column, Table, BelongsTo, Model } from "sequelize-typescript";
import { UserApplication } from "./UserApplication";

@Table
export class ApplicationPoint extends Model<ApplicationPoint> {
    @ForeignKey(() => UserApplication)
    @Column
    applicationId: number;

    @BelongsTo(() => UserApplication)
    application: UserApplication;

    @Column
    description: string;
}