import {Table, Model, Column, HasMany, Sequelize, ForeignKey} from "sequelize-typescript";
import {DbRole, Player} from "./Player";
import {TeamAchievements} from "./TeamAchievements";
import {Organization} from "./Organization";


@Table
export class Team extends Model<Team> {
    @Column
    name: string;

    @Column(Sequelize.STRING(500))
    description: string;

    @Column
    website: string;

    @Column
    region: number;

    @HasMany(() => Player, "ownedTeamId")
    owner: Player;

    @HasMany(() => Player, "teamId")
    players: Player[];

    @HasMany(() => TeamAchievements)
    achievements: TeamAchievements[];

    @ForeignKey(() => Organization)
    @Column
    organizationId: number;

    getMembers() {
        return Player.findAll({
            where: {
                [Sequelize.Op.or]: {
                    teamId: this.id,
                    ownedTeamId: this.id
                }
            }
        });
    }

    getOrgRepresentatives() {
        return Player.findAll({
            where: {
                ownedOrganizationId: this.organizationId,
                role: DbRole.Organization
            }
        });
    }
}