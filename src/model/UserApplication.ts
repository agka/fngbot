import {Table, Model, Column, HasMany, Sequelize} from "sequelize-typescript";
import { ApplicationPoint } from "./ApplicationPoint";

@Table
export class UserApplication extends Model<UserApplication> {
    @Column
    playerId: string;

    @Column
    applicationType: number; // Role

    @Column
    name: string;

    @Column(Sequelize.STRING(500))
    description: string;

    @Column
    accepted: boolean;

    @Column
    website: string;

    @Column
    email: string;

    @Column
    region: number;

    @Column // this is the "approve/deny message"
    messageSnowflake: string;

    @HasMany(() => ApplicationPoint)
    points: ApplicationPoint[];
}