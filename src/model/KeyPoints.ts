import { Table, Model, Column, BelongsTo, ForeignKey } from "sequelize-typescript";
import { Organization } from "./Organization";

@Table
export class KeyPoints extends Model<KeyPoints> {
    @Column
    description: string;

    @ForeignKey(() => Organization)
    @Column
    organizationId: number;

    @BelongsTo(() => Organization)
    organization: Organization;
}