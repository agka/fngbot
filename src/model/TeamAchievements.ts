import { Table, BelongsTo, Model, Column, ForeignKey } from "sequelize-typescript";
import { Team } from "./Team";


@Table
export class TeamAchievements extends Model<TeamAchievements> {
    @ForeignKey(() => Team)
    @Column
    teamId: number;

    @BelongsTo(() => Team)
    team: Team;
    
    @Column
    description: string;
}