import { Model, Column, Table, Sequelize } from "sequelize-typescript";
import Discord from 'discord.js';

export enum EventStatus {
    Future,
    Ongoing,
    Finished
}

@Table
export class Event extends Model<Event> {
    @Column
    name: string;

    @Column(Sequelize.STRING(500))
    description: string;

    @Column
    time: Date;

    @Column
    link: string;

    @Column
    discord: string;

    @Column
    notifyRoles: string;

    @Column
    status: number;

    @Column
    messageSnowflake: string;

    getRolesToNotify(guild: Discord.Guild) {
        const names = this.notifyRoles.split(",").map(x => x.trim().toLowerCase());
        const ret = [];
        for (let i = 0; i < names.length; i++) {
            ret.push(guild.roles.find(x => x.name.toLowerCase() == names[i]));
        }

        return ret;
    }

    get dateString() {
        return this.time.toUTCString();
    }
}

