import Discord from "discord.js";


export abstract class ChannelContext {
    protected constructor(
        public member: Discord.GuildMember, 
        public channel: Discord.TextChannel) {
        // ...
    }

    abstract respond(msg: Discord.Message): void;

    private static newChannel(prefix: string, member: Discord.GuildMember) {
        const everyoneRole = member.guild.roles.find(x => x.name == "@everyone");
        
        return member.guild.createChannel(`${prefix}-${member.id}`, "text",
         [ // Only let the specific member see this text channel.
            {
                id: member.id,
                allowed: [
                    "VIEW_CHANNEL",
                    "SEND_MESSAGES"
                ]
            },
            {
                id: everyoneRole.id,
                denied: [
                    "VIEW_CHANNEL",
                    "SEND_MESSAGES"
                ]
            }
        ]).then(x => <Discord.TextChannel>x);
    }

    static async make(
        x: any, 
        member: Discord.GuildMember, 
        prefix: string, 
        channel: Discord.TextChannel = null) {
        let chan = channel;

        if (!channel)
            chan = await ChannelContext.newChannel(prefix, member);

        return new x(member, chan);
    }
}

