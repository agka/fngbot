import Discord from "discord.js";
import {Player} from "../model/Player";
import {StatefulChannelContext} from "../stateChannelContext";
import {ChannelContext} from "../channelcontext";
import * as Prompts from "../prompts";
import {templates} from "../templates";
import {clearAndReassignMemberRoles} from "../roles";
import {PlayerAchievement} from "../model/PlayerAchievement";
import {addEmbed} from "../prompts";
import {createPlayerEmbed} from "../profiles";

enum PlayerPrompt {
    Start,
    Init,
    AskName,
    ValidateName,
    PickAchCount,
    CheckAchCount,
    AskAchievement,
    GetBio,
    ValidateBio,
    GetSocialMedia,
    PromptSatisfactionYesNo,
    GetRegion,
    Save,
    Accept
}

class RegistrationPlayer extends StatefulChannelContext {

    achievements_count = 0;
    achievements: string[] = [];

    protected player: Player;

    onSaved: () => void;

    getProfile(): string {
        return "Are you satisfied with the following?";
    }

    async savePlayer(name: string) {
        if (name !== this.player.ign)
            this.player.ign = name;

        // get rid of existing achievements if they exist
        PlayerAchievement.destroy({
            where: {
                playerId: this.player.discordId
            }
        });

        // add the new ones
        for (const achievement of this.achievements) {
            await PlayerAchievement.create({
                playerId: this.player.discordId,
                description: achievement
            });
        }

        // save the new stuff
        await this.player.save();
        if (this.onSaved)
            this.onSaved();
    }

    protected constructor(member: Discord.GuildMember, channel: Discord.TextChannel) {
        super(member, channel);

        var temp_name: string;
        this.promptStates = [
            // do welcome
            Prompts.reactYesNoBranch(PlayerPrompt.Start,
                () => templates["welcome.play"]({member: member}),
                PlayerPrompt.Init,
                PlayerPrompt.Accept),

            // Init
            Prompts.action(PlayerPrompt.Init, () => {
                this.achievements = []; // clear this up as init
            }, PlayerPrompt.AskName),

            // prompt IGN
            Prompts.stringPrompt(PlayerPrompt.AskName,
                () => templates["rplayer.q1"]({}),
                r => temp_name = r,
                PlayerPrompt.PickAchCount),

            // pick achievement count
            Prompts.reactOneTwoThreeOrNone(PlayerPrompt.PickAchCount,
                () => templates["rplayer.q2"]({}),
                c => this.achievements_count = c,
                PlayerPrompt.CheckAchCount),

            // validate count
            Prompts.condition(PlayerPrompt.CheckAchCount,
                () => this.achievements_count > 0,
                PlayerPrompt.AskAchievement,
                PlayerPrompt.GetBio),

            // get Nth achievement
            Prompts.gatherStrings(PlayerPrompt.AskAchievement,
                () => templates["rplayer.q3"]({num: this.achievements.length + 1}),
                res => {
                    this.achievements.push(res);

                    return this.achievements.length >= this.achievements_count ?
                        Prompts.PromptAction.Advance : Prompts.PromptAction.Repeat;
                },
                PlayerPrompt.GetBio),

            // get short bio
            Prompts.stringPrompt(PlayerPrompt.GetBio,
                () => templates["rplayer.q4"]({}),
                r => this.player.bio = r,
                PlayerPrompt.ValidateBio),

            // validate bio length
            Prompts.condition(PlayerPrompt.ValidateBio,
                () => this.player.bio.length >= 250,
                PlayerPrompt.GetBio,
                PlayerPrompt.GetSocialMedia),

            // get social media link
            Prompts.stringPrompt(PlayerPrompt.GetSocialMedia,
                () => templates["rplayer.q5"]({}),
                r => this.player.socialMedia = r,
                PlayerPrompt.GetRegion),

            // Get region
            Prompts.reactRegion(PlayerPrompt.GetRegion,
                () => templates["rplayer.q6"]({}),
                r => this.player.verifiedRegion = r,
                PlayerPrompt.PromptSatisfactionYesNo),

            // confirm satisfaction
            addEmbed(Prompts.reactYesNoBranch(PlayerPrompt.PromptSatisfactionYesNo,
                () => this.getProfile(),
                PlayerPrompt.Save,
                PlayerPrompt.Init),
                () => createPlayerEmbed(
                    this.player,
                    this.channel.guild,
                    this.achievements)),

            // save and finish
            Prompts.action(PlayerPrompt.Save,
                () => this.savePlayer(temp_name),
                PlayerPrompt.Accept),

            Prompts.accept(PlayerPrompt.Accept)
        ];

        this.buildTransitions();
    }

    static async construct(member: Discord.GuildMember, player: Player) {
        const ctx = ChannelContext.make(RegistrationPlayer, member, "reg");
        return ctx.then(nctx => {
            nctx.player = player;
            return <RegistrationPlayer>nctx;
        });
    }
}

// Register a player into the database with an interactive prompt.
// member is who prompted this, player is the database entry, bot is the bot instance to register
// the channel context to.
export function RegisterPlayer(
    member: Discord.GuildMember,
    player: Player) {

    const ctx = RegistrationPlayer.construct(member, player);
    return ctx.then(item => {
        item.onSaved = () => {
            clearAndReassignMemberRoles(player, member);
        }

        return item;
    });
}