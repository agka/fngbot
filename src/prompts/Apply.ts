import Discord from 'discord.js';
import {StatefulChannelContext} from "../stateChannelContext";
import {ChannelContext} from '../channelcontext';
import * as Prompts from "../prompts";
import {templates} from "../templates";
import {DbRole} from '../model/Player';
import {UserApplication} from '../model/UserApplication';
import {ApplicationPoint} from '../model/ApplicationPoint';
import {addMessageFunction} from '../applications';
import validator = require("validator");


enum ApplyState {
    Start,
    Init,
    PromptName,
    Desc,
    VerifyDesc,
    AskKPCount,
    CheckKP,
    AskKP,
    PromptContact,
    VerifyContact,
    PromptRegion,
    PromptSatisfied,
    Save,
    Accept
}

class ApplyPrompt extends StatefulChannelContext {

    application: UserApplication;
    applicationType: DbRole;

    keypoints: string[];

    get modeStr() {
        switch (this.applicationType) {
            case DbRole.Player:
                return "play";
            case DbRole.Organization:
                return "org";
            case DbRole.Team:
                return "team";
        }
    }

    getWelcome() {
        return templates[`apply.welcome.${this.modeStr}`]({});
    }

    getQuestion(q: number, arg: any = {}) {
        return templates[`apply.${this.modeStr}.q${q}`](arg);
    }

    private async saveApplication() {
        var msg: Discord.Message = null;

        // autoreject 
        if (this.keypoints.length === 0)
            this.application.accepted = false;
        else {
            msg = await this.bot.createApplicationMessage(this.application, this.keypoints);
            this.application.messageSnowflake = msg.id;
        }

        this.application.save().then(async app => {
            for (var kp of this.keypoints) {
                await ApplicationPoint.create({
                    applicationId: app.id,
                    description: kp
                });
            }
        });

        if (msg)
            await addMessageFunction(this.application, msg);
    }

    private getApplication(): string {
        return "Are you sure?";
    }

    private async createApplication() {
        this.application = await UserApplication.build({
            playerId: this.member.id,
            applicationType: this.applicationType
        })
    }

    protected constructor(
        member: Discord.GuildMember,
        channel: Discord.TextChannel) {
        super(member, channel);

        let kpcount: number;
        let urlOrEmail: string = null;

        this.promptStates = [
            // welcome and init
            Prompts.reactYesNoBranch(ApplyState.Start,
                () => this.getWelcome(),
                ApplyState.Init,
                ApplyState.Accept),

            Prompts.action(ApplyState.Init, () => {
                    this.keypoints = [];
                    urlOrEmail = null;

                }, ApplyState.PromptName
            ),

            // name
            Prompts.stringPrompt(ApplyState.PromptName,
                () => this.getQuestion(1),
                r => this.application.name = r,
                ApplyState.Desc
            ),

            Prompts.stringPrompt(ApplyState.Desc,
                () => this.getQuestion(6),
                r => this.application.description = r,
                ApplyState.VerifyDesc),

            Prompts.condition(ApplyState.VerifyDesc,
                () => this.application.description.length < 500,
                ApplyState.AskKPCount,
                ApplyState.Desc),

            // keypoints
            Prompts.reactOneTwoThreeOrNone(ApplyState.AskKPCount,
                () => this.getQuestion(2),
                r => kpcount = r,
                ApplyState.CheckKP
            ),

            Prompts.condition(ApplyState.CheckKP,
                () => kpcount > 0,
                ApplyState.AskKP,
                ApplyState.PromptContact
            ),

            Prompts.gatherStrings(ApplyState.AskKP,
                () => this.getQuestion(3, {num: this.keypoints.length + 1}),
                r => {
                    this.keypoints.push(r);

                    return this.keypoints.length < kpcount ?
                        Prompts.PromptAction.Repeat : Prompts.PromptAction.Advance;
                }, ApplyState.PromptContact),

            // verification
            Prompts.stringPrompt(ApplyState.PromptContact,
                () => this.getQuestion(4, {isNotFirstTime: urlOrEmail !== null}),
                r => {
                    urlOrEmail = r;
                }, ApplyState.VerifyContact
            ),

            Prompts.condition(ApplyState.VerifyContact, () => {
                    if (validator.isEmail(urlOrEmail)) {
                        this.application.email = urlOrEmail;
                        return true;
                    }

                    if (validator.isURL(urlOrEmail)) {
                        this.application.website = urlOrEmail;
                        return true;
                    }

                    return false;
                },
                ApplyState.PromptRegion,
                ApplyState.PromptContact
            ),

            // region
            Prompts.reactRegion(ApplyState.PromptRegion,
                () => this.getQuestion(5, {}),
                r => this.application.region = r,
                ApplyState.PromptSatisfied
            ),

            // confirm
            Prompts.reactYesNoBranch(ApplyState.PromptSatisfied,
                () => this.getApplication(),
                ApplyState.Save,
                ApplyState.Init
            ),

            Prompts.action(ApplyState.Save,
                () => this.saveApplication(),
                ApplyState.Accept
            ),

            Prompts.accept(ApplyState.Accept)
        ];

        this.buildTransitions();
    }

    static async construct(member: Discord.GuildMember, applicationType: DbRole) {
        const ctx = ChannelContext.make(ApplyPrompt, member, "app");
        return ctx.then(async c => {
            c.applicationType = applicationType;
            await c.createApplication();
            return c;
        });
    }
}

export function Apply(member: Discord.GuildMember, applicationType: DbRole): Promise<ApplyPrompt> {
    return ApplyPrompt.construct(member, applicationType).then(it => {
        return <ApplyPrompt>it;
    })
}