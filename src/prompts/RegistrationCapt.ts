import Discord from 'discord.js';
import {StatefulChannelContext} from "../stateChannelContext";
import {Team} from '../model/Team';
import {ChannelContext} from '../channelcontext';
import * as Prompts from "../prompts";
import {templates} from "../templates";
import {clearAndReassignMemberRoles} from '../roles';
import {Player} from '../model/Player';
import {RegisterPlayer} from '../prompts/RegistrationPlayer';
import {TeamAchievements} from '../model/TeamAchievements';
import {addEmbed} from "../prompts";
import {createTeamEmbed} from "../profiles";

enum TeamPrompt {
    Begin,
    Init,
    PromptName,
    PromptAchCount,
    CheckAchCount,
    PromptAch,
    PromptDescription,
    CheckDescription,
    PromptTeamSite,
    PromptRegion,
    PromptSatisfied,
    Save,
    Accept
}

class RegistrationCapt extends StatefulChannelContext {

    team: Team;
    team_points: string[];

    point_count: number;

    public onSaved: (g: Discord.Guild) => void;

    private async saveTeam() {
        await TeamAchievements.destroy({
            where: {
                "teamId": this.team.id
            }
        });

        for (const point of this.team_points) {
            TeamAchievements.create({
                teamId: this.team.id,
                description: point
            });
        }

        await this.team.save();
        if (this.onSaved)
            this.onSaved(this.channel.guild);
    }

    private getProfile() {
        return "Are you satisfied with this?";
    }

    protected constructor(
        member: Discord.GuildMember,
        channel: Discord.TextChannel) {
        super(member, channel);

        this.promptStates = [

            // do welcome
            Prompts.reactYesNoBranch(TeamPrompt.Begin,
                () => templates["welcome.team"]({member: member}),
                TeamPrompt.Init,
                TeamPrompt.Accept
            ),

            // init
            Prompts.action(TeamPrompt.Init, () => {
                    this.team_points = [];
                }, TeamPrompt.PromptName
            ),

            // prompt
            Prompts.stringPrompt(TeamPrompt.PromptName,
                () => templates["rcapt.q1"]({}),
                r => this.team.name = r,
                TeamPrompt.PromptAchCount
            ),

            // get team achievements
            Prompts.reactOneTwoThreeOrNone(TeamPrompt.PromptAchCount,
                () => templates["rcapt.q2"]({}),
                r => this.point_count = r, TeamPrompt.CheckAchCount
            ),

            // nth achievement
            Prompts.condition(TeamPrompt.CheckAchCount,
                () => this.point_count > 0,
                TeamPrompt.PromptAch,
                TeamPrompt.PromptDescription
            ),

            Prompts.gatherStrings(TeamPrompt.PromptAch,
                () => templates["rcapt.q3"]({num: this.team_points.length + 1}),
                r => {
                    this.team_points.push(r);
                    return this.team_points.length < this.point_count ?
                        Prompts.PromptAction.Repeat : Prompts.PromptAction.Advance;
                }, TeamPrompt.PromptDescription),

            // get description
            Prompts.stringPrompt(TeamPrompt.PromptDescription,
                () => templates["rcapt.q4"]({}),
                r => this.team.description = r,
                TeamPrompt.CheckDescription),

            Prompts.condition(TeamPrompt.CheckDescription,
                () => this.team.description.length > 500,
                TeamPrompt.PromptDescription,
                TeamPrompt.PromptTeamSite
            ),

            // get team website
            Prompts.stringPrompt(TeamPrompt.PromptTeamSite,
                () => templates["rcapt.q5"]({}),
                r => this.team.website = r,
                TeamPrompt.PromptRegion
            ),

            // Region
            Prompts.reactRegion(TeamPrompt.PromptRegion,
                () => templates["rcapt.q6"]({}),
                r => this.team.region = r,
                TeamPrompt.PromptSatisfied
            ),

            // are you good with this
            addEmbed(Prompts.reactYesNoBranch(TeamPrompt.PromptSatisfied,
                () => this.getProfile(),
                TeamPrompt.Save,
                TeamPrompt.Init
            ), () => createTeamEmbed(
                this.team,
                this.channel.guild,
                this.team_points)
            ),

            Prompts.action(TeamPrompt.Save, () => this.saveTeam(), TeamPrompt.Accept),
            Prompts.accept(TeamPrompt.Accept)
        ];

        this.buildTransitions();
    }

    static async construct(member: Discord.GuildMember, team: Team) {
        const ctx = ChannelContext.make(RegistrationCapt, member, "reg");
        return ctx.then(c => {
            c.team = team;
            return c;
        });
    }
}

export function RegisterCaptain(
    member: Discord.GuildMember,
    player: Player) {
    var promiseTeam: Promise<Team>;

    if (player.ownedTeamId) {
        promiseTeam = Promise.resolve(Team.findById(player.ownedTeamId));
    } else {
        promiseTeam = Promise.resolve(Team.create().then(_ => _.save()));
    }

    return promiseTeam.then(async team => {
        player.ownedTeamId = team.id;
        team.owner = player;

        await team.save();
        await player.save();

        return team;
    }).then(team => {
        return RegistrationCapt.construct(member, team).then(_ => <RegistrationCapt>_);
    }).then(c => {
        c.onSaved = async guild => {

            // update all players of guild
            const players = await c.team.getMembers();

            for (const player of players) {
                const member = guild.members.find(x => x.id == player.discordId);
                if (member != null)
                    clearAndReassignMemberRoles(player, member);
            }
        };

        c.onAccept = () => {
            RegisterPlayer(member, player).then(x => {
                x.start(c.bot);
                c.bot.addContext(x);
            });
        };

        return c;
    });

}