import Discord from "discord.js";
import {StatefulChannelContext} from "../stateChannelContext";
import {Organization} from "../model/Organization";
import {ChannelContext} from "../channelcontext";
import * as Prompts from "../prompts";
import {templates} from "../templates";
import {Player} from "../model/Player";
import {KeyPoints} from "../model/KeyPoints";
import {makeOrganizationRole, clearAndReassignMemberRoles} from "../roles";

enum OrgPrompt {
    Start,
    Init,
    PromptName,
    PromptKPCount,
    CheckKPCount,
    AskKP,
    PromptDescription,
    ValidateDescription,
    PromptSocialMedia,
    PromptEmail,
    PromptRegion,
    PromptConfirm,
    Save,
    Accept
}

class RegistrationOrg extends StatefulChannelContext {

    org: Organization;

    keypoint_count: number;
    keypoints: string[];


    private getProfile() {
        return "Are you sure?";
    }

    onSaved: () => void;

    private async saveOrg() {

        // remove existing key points first
        await KeyPoints.destroy({
            where: {
                "organizationId": this.org.id
            }
        });

        // add new ones
        for (const kp of this.keypoints) {
            KeyPoints.create({
                organizationId: this.org.id,
                description: kp
            });
        }

        await this.org.save();

        await makeOrganizationRole(this.member.guild, this.org);

        if (this.onSaved)
            this.onSaved();
    }

    protected constructor(
        member: Discord.GuildMember,
        channel: Discord.TextChannel) {
        super(member, channel);

        this.promptStates = [


            // do welcome
            Prompts.reactYesNoBranch(OrgPrompt.Start,
                () => templates["welcome.org"]({member: member}),
                OrgPrompt.Init,
                OrgPrompt.Accept),

            // init
            Prompts.action(OrgPrompt.Init, () => {
                this.keypoints = [];
            }, OrgPrompt.PromptName),

            // prompt the name
            Prompts.stringPrompt(OrgPrompt.PromptName,
                () => templates["rorg.q1"]({}),
                r => this.org.name = r,
                OrgPrompt.PromptKPCount),

            // reaction count
            Prompts.reactOneTwoThreeOrNone(OrgPrompt.PromptKPCount,
                () => templates["rorg.q2"]({}),
                c => this.keypoint_count = c,
                OrgPrompt.CheckKPCount),

            // validate count
            Prompts.condition(OrgPrompt.CheckKPCount,
                () => this.keypoint_count > 0,
                OrgPrompt.AskKP,
                OrgPrompt.PromptDescription),

            // gather key points
            Prompts.gatherStrings(OrgPrompt.AskKP,
                () => templates["rorg.q3"]({num: this.keypoints.length + 1}),
                r => {
                    this.keypoints.push(r);

                    if (this.keypoints.length < this.keypoint_count)
                        return Prompts.PromptAction.Repeat;
                    else
                        return Prompts.PromptAction.Advance;
                },
                OrgPrompt.PromptDescription
            ),

            // description
            Prompts.stringPrompt(OrgPrompt.PromptDescription,
                () => templates["rorg.q4"]({}),
                r => this.org.description = r,
                OrgPrompt.ValidateDescription),

            // validate
            Prompts.condition(OrgPrompt.ValidateDescription,
                () => this.org.description.length > 500,
                OrgPrompt.PromptDescription,
                OrgPrompt.PromptSocialMedia),

            // social media
            Prompts.stringPrompt(OrgPrompt.PromptSocialMedia,
                () => templates["rorg.q5"]({}),
                r => this.org.website = r,
                OrgPrompt.PromptEmail),

            // email
            Prompts.stringPrompt(OrgPrompt.PromptEmail,
                () => templates["rorg.q6"]({}),
                r => this.org.email = r,
                OrgPrompt.PromptRegion),

            // region
            Prompts.reactRegion(OrgPrompt.PromptRegion,
                () => templates["rorg.q7"]({}),
                r => this.org.region = r,
                OrgPrompt.PromptConfirm),

            // confirm
            Prompts.reactYesNoBranch(OrgPrompt.PromptConfirm,
                () => this.getProfile(),
                OrgPrompt.Save,
                OrgPrompt.Init),

            // save and accept
            Prompts.action(OrgPrompt.Save,
                () => this.saveOrg(),
                OrgPrompt.Accept),

            Prompts.accept(OrgPrompt.Accept)
        ];

        this.buildTransitions();
    }

    static async construct(member: Discord.GuildMember, org: Organization) {
        const ctx = ChannelContext.make(RegistrationOrg, member, "reg")
            .then(x => <RegistrationOrg>x)
            .then(x => {
                x.org = org;
                return x;
            });
        return ctx;
    }
}

export function RegisterOrganization(
    member: Discord.GuildMember,
    player: Player) {
    var promiseOrg: Promise<Organization>;

    if (player.ownedOrganizationId)
        promiseOrg = Promise.resolve(Organization.findById(player.ownedOrganizationId));
    else {
        // convert to regular promise
        promiseOrg = Promise.resolve(Organization.create());
    }

    return promiseOrg.then(async org => {
        player.ownedOrganizationId = org.id;

        await org.save();
        await player.save();

        return org;
    }).then(org => {
        return RegistrationOrg.construct(member, org);
    }).then(reg => {

        // upon finishing, set the representative roles
        reg.onSaved = () => {
            clearAndReassignMemberRoles(player, member);
        };

        return reg;
    });
}