import Discord from 'discord.js';
import { StatefulChannelContext } from "../stateChannelContext";
import { ChannelContext } from '../channelcontext';
import * as Prompts from "../prompts";
import { templates } from "../templates";
import { Event, EventStatus } from '../model/Event';
import validator = require("validator");

class EventPrompt extends StatefulChannelContext {

    event: Event;

    day: number;
    month: number;
    year: number;

    hour: number;
    minute: number;

    isSatisfiedText() {
        const eventText = templates["event.announcement"]({event: this.event});
        return templates["event.q8"]({eventText: eventText});
    }

    async saveEvent() {
        this.event.status = EventStatus.Future;
        this.event = await this.event.save(); // get the ID

        this.bot.repostEventMessage(this.event);
    }

    protected constructor(member: Discord.GuildMember, channel: Discord.TextChannel) {
        super(member, channel);

        this.event = Event.build({});

        let notFirstDateAttempt = false;
        let validDate = false;

        let notFirstTimeAttempt = false;
        let validTime = false;

        let notFirstDiscordAttempt = false;

        const dateRe = /(\d\d\d\d)-(\d\d)-(\d\d)/;

        let notFirstLinkAttempt = false;

        const timeRe = /(\d\d):(\d\d)/;

        let satisfied = false;

        let nonExistentRoles: string[] = null;
        this.promptStates = [
            
            // name/title
            Prompts.stringPrompt(0, 
                () => templates["event.q1"]({member: this.member}),
                r => this.event.name = r, 1),
            
            // description
            Prompts.stringPrompt(1, 
                () => templates["event.q2"]({}),
                r => this.event.description = r, 2),
            Prompts.condition(2, () => this.event.description.length > 500, 1, 3),
            
            // date
            Prompts.stringPrompt(3, 
                () => templates["event.q3"]({notFirstDateAttempt: notFirstDateAttempt}),
                r => {
                    var match;
                    notFirstDateAttempt = true;
                    if ( (match = dateRe.exec(r)) != null) {
                        validDate = true;
                        this.year = parseInt(match[1]);
                        this.month = parseInt(match[2]) - 1;
                        this.day = parseInt(match[3]);
                    } else {
                        validDate = false;
                    }
                }, 4),
            Prompts.condition(4, () => validDate, 5, 3),
            
            // time (hhmm)
            Prompts.stringPrompt(5, 
                () => templates["event.q4"]({notFirstTimeAttempt: notFirstTimeAttempt}),
                r => {
                    var match;
                    notFirstTimeAttempt = true;
                    if ( (match = timeRe.exec(r)) != null) {
                        validTime = true;
                        this.hour = parseInt(match[1]);
                        this.minute = parseInt(match[2]);

                        this.event.time = new Date(Date.UTC(this.year, this.month, this.day, this.hour, this.minute));
                        
                    } else {
                        validTime = false;
                    }
                }, 6),
            Prompts.condition(6, () => validTime, 7, 5),
            
            // event link
            Prompts.stringPrompt(7, 
                () => templates["event.q5"]({notFirstLinkAttempt: notFirstLinkAttempt}),
                r => {
                    notFirstLinkAttempt = true;
                    this.event.link = r;
                }, 8),
            Prompts.condition(8, () => validator.isURL(this.event.link), 9, 7),
            
            // discord link
            Prompts.stringPrompt(9, 
                () => templates["event.q6"]({notFirstDiscordAttempt: notFirstDiscordAttempt}),
                r => {
                    notFirstDiscordAttempt = true;
                    this.event.discord = r;
                }, 10),
            Prompts.condition(10, () => {
                if (validator.isURL(this.event.discord) || this.event.discord.toLowerCase() === "none") {
                    if(this.event.discord === "none")
                        this.event.discord = null;
                    return true;
                }
                return false;
            }, 11, 9),

            // roles to notify
            Prompts.stringPrompt(11, 
                () => templates["event.q7"]({nonExistentRoles: nonExistentRoles}), 
                r => {
                    var roleNames = r.split(",").map(x => x.trim());

                    nonExistentRoles = [];
                    for (var role of roleNames) {
                        const lc = role.toLowerCase();
                        const discordRole = this.member.guild.roles.find(x => x.name.toLowerCase() == lc);
                        if (!discordRole)
                            nonExistentRoles.push(role);
                    }

                    this.event.notifyRoles = r;
            }, 12),
            Prompts.condition(12, () => nonExistentRoles.length === 0, 13, 11),

            // do you like how the event announcement looks?
            Prompts.reactYesNo(13, 
                () => this.isSatisfiedText(), 
                y => satisfied = y, 14),
            Prompts.condition(14, () => satisfied, 100, 0),

            // save and finish
            Prompts.action(100, () => this.saveEvent(), 101),
            Prompts.accept(101)
        ];

        this.buildTransitions();
    }

    static async construct(member: Discord.GuildMember) {
        const ctx = ChannelContext.make(EventPrompt, member, "evt");
        return ctx.then(_ => <EventPrompt>_);
    }
}

export function CreateEvent(member: Discord.GuildMember) {
    return EventPrompt.construct(member);
}