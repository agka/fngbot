import { AddCommand } from "./commands/AddCommand";
import { PingCommand, Command } from './commandbase';
import { ProfileCommand } from "./commands/ProfileCommand";
import { VerifyCommand } from "./commands/VerifyCommand";
import { RemoveCommand } from "./commands/RemoveCommand";
import { ApplyCommand } from "./commands/ApplyCommand";
import { EventCommand } from "./commands/EventCommand";
import { UpdateCommand } from "./commands/UpdateCommand";
import { PauseCommand } from "./commands/PauseCommand";
import { ResumeCommand } from "./commands/ResumeCommand";
import { AddRepCommand } from "./commands/AddRepCommand";
import { OwnerCommand } from "./commands/OwnerCommand";
import { RoleAddCommandCommand } from "./commands/RoleAddCommandCommand";
import { RoleRemoveCommandCommand } from "./commands/RoleRemoveCommandCommand";
import { DisbandCommand } from "./commands/DisbandCommand";
import { EndCommand } from "./commands/EndCommand";
import { AddTeamCommand } from "./commands/AddTeamCommand";
import { GetRolesCommand } from "./commands/GetRolesCommand";
import {InfoCommand} from "./commands/InfoCommand";
// import {FloodChannelsCommand} from "./commands/FloodChannelsCommand";

// Add commands here!
export var commandList = [
    PingCommand,
    AddCommand,
    ProfileCommand,
    VerifyCommand,
    RemoveCommand,
    ApplyCommand,
    EventCommand,
    UpdateCommand,
    PauseCommand,
    ResumeCommand,
    AddRepCommand,
    OwnerCommand,
    RoleAddCommandCommand,
    RoleRemoveCommandCommand,
    DisbandCommand,
    EndCommand,
    AddTeamCommand,
    GetRolesCommand,
    InfoCommand,
    // FloodChannelsCommand // dangerous command
].map(x => new x());

// Inner stuff.
export var commandMap: { [cmdString: string]: Command; } = {};

for (const cmd of commandList) {
    if (commandMap[cmd.commandString])
    {
        console.warn(`Overwriting existing command, ${cmd.commandString}`);
    }

    commandMap[cmd.commandString] = cmd;
}

export function commandExists(cmd: string) {
    return commandMap[cmd] !== null && commandMap[cmd] !== undefined;
}