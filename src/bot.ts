import Discord, {RichEmbed} from "discord.js";
import {checkVerifiedRole, verifyRolesExist} from "./roles";
import {DbRole, Player, Region} from "./model/Player";
import {dispatchCommand} from "./runcommand";
import {ChannelContext} from "./channelcontext";
import {RegisterPlayer} from "./prompts/RegistrationPlayer";
import {loadTemplates, templates} from "./templates";
import {RegisterOrganization} from "./prompts/RegistrationOrg";
import {RegisterCaptain} from "./prompts/RegistrationCapt";
import {StatefulChannelContext} from "./stateChannelContext";
import {UserApplication} from "./model/UserApplication";
import {Event, EventStatus} from "./model/Event";

function regionToString(reg: Region) {
    switch (reg) {
        case Region.EU:
            return "EU";
        case Region.NA:
            return "NA";
        case Region.OCE:
            return "OCE";
        case Region.SEA:
            return "SEA";
        default:
            return "unknown";
    }
}

function dbRoleToString(applicationType: DbRole) {
    switch (applicationType) {
        case DbRole.Organization:
            return "Organization";
        case DbRole.Player:
            return "Player";
        case DbRole.Team:
            return "Team";
        default:
            return "unknown";
    }
}

export class FngBot {
    channelContexts: { [channelId: string]: ChannelContext } = {};
    commandToken = "-";

    allowApplications = true;

    applicationsChannel: Discord.TextChannel;

    // event channels
    ongoingChannel: Discord.TextChannel;
    futureChannel: Discord.TextChannel;
    endedChannel: Discord.TextChannel;

    onMemberAdd(member: Discord.GuildMember) {
        console.log(`${member.id} joined ${member.guild.name}`);
        checkVerifiedRole(member)
            .then((player) => this.promptProfileCreation(player, member));
    }

    addContext(ctx: ChannelContext) {
        this.channelContexts[ctx.channel.id] = ctx;
    }

    removeContext(ctx: ChannelContext) {
        this.channelContexts[ctx.channel.id] = null;
    }

    async createApplicationMessage(app: UserApplication, keypoints: string[]) {
        const embed = new RichEmbed();
        embed.setTitle(`${dbRoleToString(app.applicationType)} application`);
        embed.setAuthor(app.name);
        embed.setDescription(app.description);
        embed.addField("Verification", app.email || app.website, true);
        embed.addField("Region", regionToString(app.region));

        embed.addField("Key Points/Achievements", keypoints.join("\n"));

        return <Discord.Message>await this.applicationsChannel.send("Application inbound: ", {embed});
    }

    promptProfileCreation(player: Player, member: Discord.GuildMember) {

        if (!player || player.verifiedRegion == null)
            return;

        var chan: Promise<StatefulChannelContext>;

        // console.log(`Dealing profile creation for role ${player.role} for ${member.id}`);
        switch (player.role) {
            case DbRole.Organization:
                chan = RegisterOrganization(member, player);
                break;
            case DbRole.Player:
                chan = RegisterPlayer(member, player);
                break;
            case DbRole.Team:
                chan = RegisterCaptain(member, player);
                break;
            default:
                console.log(`Warning - unknown role ${player.role} assigned to ${player.discordId}`);
        }

        if (!chan) return;

        chan.then(ch => {
            this.addContext(ch);
            ch.start(this);
        });
    }

    async repostEventMessage(e: Event) {
        const msg = this.client.channels.filter(x => x.type == "text")
            .map(x => <Discord.TextChannel>x)
            .map(x => x.messages.find(m => m.id === e.messageSnowflake));

        for (var m of msg) {
            if (m != null)
                m.delete();
        }

        // repost message
        let channel: Discord.TextChannel;
        switch (e.status) {
            case EventStatus.Finished:
                channel = this.endedChannel;
                break;
            case EventStatus.Future:
                channel = this.futureChannel;
                break;
            case EventStatus.Ongoing:
                channel = this.ongoingChannel;
                break;
        }

        const roles = e.getRolesToNotify(channel.guild);
        const oldMentionable = roles.map(x => x.mentionable);
        const notifyStr = roles.map(x => x.toString()).join(" ");

        // make mentionable
        for (let role of roles) {
            await role.edit({mentionable: true});
        }

        // send message, save it
        const evtMessage = <Discord.Message> (await channel.send(
            `${notifyStr}, `
            + '\n'
            + templates["event.announcement"]({event: e})
        ));

        e.messageSnowflake = evtMessage.id;
        await e.save();

        // make old mentionable status
        for (let idx in roles) {
            const role = roles[idx];
            await role.edit({mentionable: oldMentionable[idx]});
        }
    }

    onReady() {
        const chan = this.client.channels.find(x => x.id === this.config.applicationsChannel);
        this.applicationsChannel = <Discord.TextChannel>chan;

        const ogChan = this.client.channels.find(x => x.id === this.config.eventChannels.ongoing);
        this.ongoingChannel = <Discord.TextChannel>ogChan;


        const fuChan = this.client.channels.find(x => x.id === this.config.eventChannels.future);
        this.futureChannel = <Discord.TextChannel>fuChan;

        const endChan = this.client.channels.find(x => x.id === this.config.eventChannels.ended);
        this.endedChannel = <Discord.TextChannel>endChan;
    }

    async onMessage(message: Discord.Message) {
        if (message.author.bot) return;

        // console.log(message.content);

        const wasDispatched = await dispatchCommand(this.commandToken, message, this);
        if (wasDispatched) {
            return; // don't bother with channel dispatch
        }

        this.dispatchChannel(message);
    }

    dispatchChannel(message: Discord.Message) {
        const ctx = this.channelContexts[message.channel.id];
        if (!ctx) return;

        ctx.respond(message);
    }

    setupClient() {
        this.client.guilds.array().forEach((guild) => {
            verifyRolesExist(this.client, guild);
        });

        console.info("Finished. Registering callbacks.");
        this.client.on("guildMemberAdd", this.onMemberAdd.bind(this));
        this.client.on("message", this.onMessage.bind(this));

        this.onReady();
        console.info("Fired up and ready to serve.");
    }

    constructor(private client: Discord.Client, private config: any) {
        loadTemplates();
        this.setupClient();
    }


}