import Discord, {RichEmbed} from 'discord.js';
import {Team} from "./model/Team";
import {DbRole, Player} from "./model/Player";
import {Organization} from "./model/Organization";
import {TeamAchievements} from "./model/TeamAchievements";
import {PlayerAchievement} from "./model/PlayerAchievement";
import {KeyPoints} from "./model/KeyPoints";

// player must have ign to call this function
export async function createPlayerEmbed(
    player: Player,
    guild: Discord.Guild,
    keypoints?: string[]): Promise<RichEmbed> {
    const embed = new RichEmbed();
    const kp = keypoints || (await PlayerAchievement.findAll({
        where: {
            playerId: player.discordId
        }
    })).map(x => x.description);

    embed.setTitle(player.ign);
    embed.setDescription(player.bio);

    const team = await Team.findOne({
        where: {
            id: player.effectiveTeamId
        }
    });


    if (team) {
        if (player.role == DbRole.Team) {
            embed.addField("Team Captain for", team.name);
        } else {
            embed.addField("Team", team.name);
        }
    }

    if (kp.length > 0) {
        const achievements = kp.join("\n");
        embed.addField(`Achievements`, achievements, true);
    }

    embed.addField("Social Media", player.socialMedia, true);

    return embed;
}

export async function createTeamEmbed(
    team: Team,
    guild: Discord.Guild,
    keypoints?: string[]): Promise<RichEmbed> {
    const embed = new RichEmbed();
    embed.setAuthor(`${team.name}`);


    embed.setDescription("```" + team.description + "```");
    embed.setFooter(`Team ID: ${team.id}`);

    const kp = keypoints || (await TeamAchievements.findAll({
        where: {
            teamId: team.id
        }
    })).map(x => x.description);

    if (team.organizationId != null) {
        const org = await Organization.findOne({
            where: {
                id: team.organizationId
            }
        });

        if (org) {
            embed.setTitle(`Owned by ${org.name}`);
            embed.setURL(`${org.website}`);
            embed.addField("Organization", org.name, true);
        }
    }

    const playerList = await team.getMembers();

    const players = playerList.map(x => {
        const name = x.ign || "_No profile yet_";
        const suffix = x.role === DbRole.Player ? "" : " **(Captain)**";
        return `${name}${suffix}`;
    }).join("\n");

    embed.addField("Players (In-Game Name)", players, true);

    const playersDiscordTags = (await Promise.all(playerList.map(x => {
        return guild.client.fetchUser(x.discordId)
            .then(x => x.tag)
            .catch(() => {
                return x.discordId;
            });
    }))).join("\n");

    embed.addField("Discord ID", playersDiscordTags, true);

    const repros = await team.getOrgRepresentatives();

    if (repros.length > 0) {
        const reproStr = (await Promise.all(repros.map(x => {
            return guild.client.fetchUser(x.discordId).then(x => x.tag);
        }))).join("\n");
        embed.addField("Organization Representatives", reproStr);
    }

    if (kp.length > 0) {
        const achievements = kp.join("\n");
        embed.addField(`Achievements`, achievements);
    }

    return embed;
}

export async function createOrgEmbed(org: Organization): Promise<RichEmbed> {
    const embed = new RichEmbed();

    const kp = await KeyPoints.findAll({
        where: {
            organizationId: org.id
        }
    });

    if (kp.length > 0) {
        const achievements = kp.map(x => x.description).join("\n");
        embed.addField(`Key Points`, achievements);
    }

    return embed;
}