import Discord from "discord.js";
import {Player, DbRole, Region} from "./model/Player";
import {Team} from "./model/Team";
import {Organization} from "./model/Organization";

const config = require("../config.json");

export enum ServerRole {
    Admin,
    Staff,
    Representative,
    Professional,
    Verified,
    Upcoming,
    FreeAgent,
    Captain,
    VerifiedNA,
    VerifiedEU,
    VerifiedSEA,
    VerifiedOCE
}

export function dbRoleFromCharacter(s: string): DbRole {
    s = s.toLowerCase();
    switch (s) {
        case "t":
            return DbRole.Team;
        case "o":
            return DbRole.Organization;
        case "p":
            return DbRole.Player;
        default:
            return null;
    }
}

// creates and saves the role for this organization
export function makeOrganizationRole(
    guild: Discord.Guild,
    organization: Organization): Promise<Discord.Role> {

    if (organization.roleSnowflake) {
        const role = guild.roles.find(x => x.id === organization.roleSnowflake);

        if (!role) {
            console.warn(`Role snowflake for organization ${organization.name} set, but not found!`);
            return;
        }

        if (role.name != organization.name) {
            role.setName(organization.name);
        }

        return Promise.resolve(role);
    }

    const upcoming = getRole(guild, ServerRole.Upcoming);

    return guild.createRole({
        name: organization.name,
        mentionable: true,
        hoist: true,
        position: upcoming.calculatedPosition + 1
    }).then(async role => {
        organization.roleSnowflake = role.id;
        await organization.save();

        return role;
    });
}

function getOrganizationRole(orgId: number) {
    return Organization.findById(orgId).then(org => org.roleSnowflake);
}

/*function isHigherRole(member: Discord.GuildMember, role: Discord.Role)
{
    const me = member.guild.members.find(x => x.client.user.id == member.client.user.id);
    const v = me.highestRole.comparePositionTo(role) <= 0;
    console.log(v + " " + role.name);

    return v;
}*/

async function removeAllRoles(member: Discord.GuildMember) {
    const upcoming = getRole(member.guild, ServerRole.Upcoming);

    for (const role of member.roles) {
        // lower role than upcoming
        if (role[1].calculatedPosition < upcoming.calculatedPosition) {
            continue;
        }

        if (role[1].name !== "@everyone") {
            // console.log("remove " + role[1].name);
            await member.removeRole(role[1]).catch(cause => {
                console.error(cause);
                console.error(`Role: ${role[1].name} ============`);
            });
        }
    }
}

export function verifyRolesExist(client: Discord.Client, guild: Discord.Guild) {
    console.log(`Verifying roles for guild ${guild.name}`);

    // "builtin"
    for (const roleIndex of Object.keys(ServerRole)) {
        // omit numeric indices
        if (!isNaN(parseFloat(roleIndex))) continue;

        // check role
        const role = config.roles[roleIndex];

        if (!guild.roles.some(x => x.id == role)) {
            console.log(guild.roles.array().map(x => [x.name, x.id]));
            throw new Error(`Configured role id for '${roleIndex}' doesn't exist. Exiting bot.`);
        }

    }

    // teams

    // orgs
}

export function getRole(guild: Discord.Guild, role: ServerRole): Discord.Role {
    return guild.roles.find((val) => val.id == config.roles[ServerRole[role]]);
}

export function hasServerRole(member: Discord.GuildMember, role: ServerRole) {
    const roleToCompare = getRole(member.guild, role);
    return member.roles.find(x => x.id === roleToCompare.id) != null;
}

function addServerRole(member: Discord.GuildMember, role: ServerRole) {
    const roleToAdd = getRole(member.guild, role);
    if (!member.roles.has(roleToAdd.id)) {
        // console.info(`Adding role '${role}' to ${member.id}`);
        return member.addRole(roleToAdd);
    }

    return Promise.resolve();
}

function removeServerRole(member: Discord.GuildMember, role: ServerRole) {
    const roleToRemove = getRole(member.guild, role);
    if (member.roles.has(roleToRemove.id)) {
        console.info(`Taking role '${role}' from ${member.id}`);
        member.removeRole(roleToRemove);
    }
}

// always called for all incoming users
export function checkVerifiedRole(member: Discord.GuildMember) {
    return Player.find({
        where: {
            discordId: member.id
        }
    }).then((player) => {
        // console.log(player);
        if (player && player.verifiedRegion != null) {
            // already in database? act now
            addServerRole(member, ServerRole.Verified);
            removeServerRole(member, ServerRole.Upcoming);

            addRegionRole(player, member);

        } else {
            // not in database, or not verified
            addServerRole(member, ServerRole.Upcoming);
            removeServerRole(member, ServerRole.Verified);
        }

        return player;
    });
}

async function addRegionRole(player: Player, member: Discord.GuildMember) {
    let region = player.verifiedRegion;

    if (player.hasTeam) {
        const team = await Team.findById(player.effectiveTeamId);
        region = team.region || region;
    }

    if (player.hasOrganization) {
        const org = await Organization.findById(player.effectiveOrganizationId);
        region = org.region || region;
    }

    switch (region) {
        case Region.NA:
            addServerRole(member, ServerRole.VerifiedNA);
            break;
        case Region.EU:
            addServerRole(member, ServerRole.VerifiedEU);
            break;
        case Region.OCE:
            addServerRole(member, ServerRole.VerifiedOCE);
            break;
        case Region.SEA:
            addServerRole(member, ServerRole.VerifiedSEA);
            break;
    }
}

// if user is registered as player (no-op for those that are not players)
function checkPlayerRole(player: Player, member: Discord.GuildMember): boolean {

    // team capts also get player roles
    if (player.role == DbRole.Organization) {
        return;
    }

    player.teamHasOrganization().then(hasTeamOrg => {
        if (player.hasOrganization || hasTeamOrg) {
            addServerRole(member, ServerRole.Professional);

            // can't be an owner and a player
            if (player.organizationId) {
                addOrganizationRole(player, member);
            } else {
                // console.warn(`${member.id} is an organization owner, but is also a player or a captain?`);
            }
        } else {
            removeServerRole(member, ServerRole.Professional);

            if (player.organizationId) {
                removeOrganizationRole(player, member);
            }
        }

        if (player.hasTeam || player.role == DbRole.Team) {
            const team = Team.findById(player.effectiveTeamId);

            team.then(t => {
                const name = player.ign || member.user.username;
                if (t)
                    member.setNickname(`[${t.name}] ${name}`);
                else
                    member.setNickname(name);
            });

            return true;
        } else {
            addServerRole(member, ServerRole.FreeAgent);

            const name = player.ign || member.user.username;
            member.setNickname(name);

            return false;
        }
    });
}

// if user is registered as organization (no-op for non-organizations)
async function checkOrganizationRole(
    player: Player,
    member: Discord.GuildMember) {
    if (player.role != DbRole.Organization) {
        console.warn(`checkOrganizationRole called for non-organization ${member.id}`);
        return false;
    }

    if (player.hasOrganization) {
        if (player.ownedOrganizationId)
            await addServerRole(member, ServerRole.Representative);

        await addOrganizationRole(player, member);
    } else {
        removeServerRole(member, ServerRole.Representative);
    }
}

function addOrganizationRole(player: Player, member: Discord.GuildMember) {
    return getOrganizationRole(player.effectiveOrganizationId).then(roleId => {
        const role = member.guild.roles.find(it => it.id === roleId);

        if (!role) {
            console.warn(`Organization role does not exist for org ${player.effectiveOrganizationId}`);
            return;
        }

        if (!member.roles.has(role.id))
            member.addRole(role);
    });
}

function removeOrganizationRole(player: Player, member: Discord.GuildMember) {
    return getOrganizationRole(player.effectiveOrganizationId).then(roleId => {
        const role = member.guild.roles.find(it => it.id === roleId);

        if (!role) {
            console.warn(`Organization role does not exist for org ${player.effectiveOrganizationId}`);
            return;
        }

        if (member.roles.has(role.id))
            member.removeRole(role);
    });
}

async function checkCaptRole(
    player: Player,
    member: Discord.GuildMember) {
    if (player.role != DbRole.Team) {
        return;
    }

    const team = await Team.findById(player.effectiveTeamId);

    addServerRole(member, ServerRole.Captain);

    if (team === null) {
        console.warn(`NULL team for captain ${member.id} name won't be set`);
        return;
    }

    var name: string;

    if (player.ign && player.ign !== "" && player.ign !== null)
        name = player.ign;
    else
        name = member.user.username;

    member.setNickname(`[${team.name}] ${name}`);
}

export async function clearAndReassignMemberRoles(player: Player, member: Discord.GuildMember) {
    await removeAllRoles(member);

    if (player) {
        switch (player.role) {
            case DbRole.Player:
                await checkPlayerRole(player, member);
                break;

            case DbRole.Organization:
                await checkOrganizationRole(player, member);
                break;

            case DbRole.Team:
                await checkCaptRole(player, member);
                await checkPlayerRole(player, member);
                break;
        }
    } else {
        if (member.displayName != member.user.username) {
            member.setNickname(member.user.username);
        }
    }

    return checkVerifiedRole(member);
}