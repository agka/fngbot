
import { ServerRole } from "./roles";
import Discord from "discord.js";
import { FngBot } from "./bot";
import { hasServerRole } from "./roles";
import { CommandPermission } from "./model/CommandPermissions";
import { Sequelize } from "sequelize-typescript";

export abstract class Command {

    matchSnowflakes(content: string) {
        const re = /(?:\W|^)(?:<@)?(\d+)(?:>)?/g;
        const ret: string[] = [];

        let match;
        while( (match = re.exec(content)) !== null) {
            ret.push(match[1]);
        }

        return ret;
    }

    protected constructor(
        public commandString: string, 
        public allowedRoles: ServerRole[],
        private isStaffCommand: boolean) {

    }

    async isAllowed(member: Discord.GuildMember): Promise<boolean> {
        if (!this.isStaffCommand)
            return this.allowedRoles.some(x => hasServerRole(member, x));
        else {
            // admin and server owner run whatever they want, staff
            // only if they have the permission
            if (hasServerRole(member, ServerRole.Admin) || member.guild.ownerID === member.id) {
                return true;
            } else if (hasServerRole(member, ServerRole.Staff)) {
                return CommandPermission.findOne({
                    where: {
                        command: this.commandString,
                        roleSnowflake: {
                            [Sequelize.Op.in]: member.roles.array().map(x => x.id)
                        }
                    }
                }).then(x => {
                    return x != null;
                });
            }

            // allow commands like -end to be run by non-staff
            return this.allowedRoles.some(x => hasServerRole(member, x));
        }
    }

    onCommand(
        msg: Discord.Message, 
        content: string, 
        bot: FngBot, 
        isStaffCaller: boolean) {
        this.action(content, msg, bot, isStaffCaller);
    }

    protected abstract action(
        content: string, 
        msg: Discord.Message, 
        bot: FngBot,
        isStaffCaller: boolean): void;
}

export class PingCommand extends Command {
    constructor() {
        super("ping", [], true);
    }

    action(
        content: string, 
        msg: Discord.Message, 
        bot: FngBot,
        isStaffCaller: boolean) {

        msg.reply("pong");
    }
}
