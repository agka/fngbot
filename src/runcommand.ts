import { commandMap } from "./commandList";
import Discord from "discord.js";
import { FngBot } from "./bot";
import { hasServerRole, ServerRole } from "./roles";


export async function dispatchCommand(commandToken: string, msg: Discord.Message, bot: FngBot): Promise<boolean> {
    // "parse"
    if (msg.content.indexOf(commandToken) != 0)
        return false;

    const all_ctnt = msg.content.substr(1);

    const cntindex = all_ctnt.indexOf(" ");

    let cmdstr: string;
    let content = "";
    if (cntindex != -1) {
        cmdstr = all_ctnt.substr(0, cntindex);
        content = all_ctnt.substr(cntindex + 1);
    }
    else
        cmdstr = all_ctnt;

    // execute
    const command = commandMap[cmdstr];
    if (!command) {
        // console.debug("command does not exist");
        return false;
    }

    const allowed = await command.isAllowed(msg.member); 
    if (!allowed) {
        // console.debug("not authorized");
        return false;
    }

    const isStaffOrAdmin = hasServerRole(msg.member, ServerRole.Admin) ||
                           hasServerRole(msg.member, ServerRole.Staff);
    command.onCommand(msg, content, bot, isStaffOrAdmin);
    return true;
} 