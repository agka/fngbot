import Handlebars from "handlebars";
import fs from 'fs';

export var templates:{[name: string]: HandlebarsTemplateDelegate<any>} = {
};

function noExt(path: string) {
    return path.substr(0, path.lastIndexOf(".txt"));
}

export function loadTemplates() {
    console.log("Loading response templates...");
    fs.readdir("templates", (err, items) => {
        for (var file of items) {
            const contents = fs.readFileSync("templates/"+file, {encoding: "utf-8"});
            templates[noExt(file)] = Handlebars.compile(contents, {
                noEscape: true
            });
        }
    });
    console.log("Done.");
}



