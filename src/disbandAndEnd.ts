import { Player, DbRole } from "./model/Player";
import { Sequelize } from "sequelize-typescript";
import { clearAndReassignMemberRoles } from "./roles";
import Discord from "discord.js";
import { Team } from "./model/Team";
import { Organization } from "./model/Organization";
import { TeamAchievements } from "./model/TeamAchievements";
import { KeyPoints } from "./model/KeyPoints";

async function transferTeamOwnershipOrDestroyTeam(captain: Player, guild: Discord.Guild) {
    // has team, transfer ownership
    var candidate = await Player.findOne({
        where: {
            discordId: {
                [Sequelize.Op.ne]: captain.discordId
            },
            
            verifiedRegion: {
                [Sequelize.Op.ne]: null
            },
            
            [Sequelize.Op.or]: {
                teamId: captain.effectiveTeamId,
                ownedTeamId: null
            },
            role: DbRole.Player
        }
    });

    if (candidate) {
        // transfer team capt
        await Player.update({
            teamId: null,
            ownedTeamId: captain.ownedTeamId,
            role: DbRole.Team
        }, {
                where: {
                    discordId: candidate.discordId
                }
            }
        );

        // update candidate
        candidate.teamId = null;
        candidate.ownedTeamId = captain.ownedTeamId;
        candidate.role = DbRole.Team;

        const candidateMember = guild.members.find(x => x.id == candidate.discordId);
        if (candidateMember) {
            await clearAndReassignMemberRoles(candidate, candidateMember);
        }
    } else {
        // disband team
        const team = await Team.findById(captain.ownedTeamId);
        await disbandTeam(team, guild);
    }
}

async function destroyOrgIfNoReps(rep: Player, guild: Discord.Guild) {
    const players = await Player.findAll({
        where: {
            [Sequelize.Op.or]: {
                organizationId: rep.ownedOrganizationId,
                ownedOrganizationId: rep.ownedOrganizationId
            },
            discordId: {
                [Sequelize.Op.ne]: rep.discordId 
            }
        }
    });

    // no other representatives or no other members
    if (!players.some(x => x.role == DbRole.Organization) || players.length == 0) {
        const org = await Organization.findById(rep.ownedOrganizationId);
        await disbandOrganization(org, guild);
    }
}

export async function endPlayer(player: Player, guild: Discord.Guild) {
    if (player.role == DbRole.Team) {
        if (player.hasTeam) {
            await transferTeamOwnershipOrDestroyTeam(player, guild);
        }
    }

    if (player.role == DbRole.Organization) {
        if (player.hasOrganization)
            await destroyOrgIfNoReps(player, guild);
    }

    const member = guild.members.find(x => x.id == player.discordId);
    await player.destroy();
    if (member)
        await clearAndReassignMemberRoles(null, member);
}

export async function disbandTeam(team: Team, guild: Discord.Guild) {
    const who = {
        where: {
            [Sequelize.Op.or]: {
                teamId: team.id,
                ownedTeamId: team.id
            }
        }
    };

    const players = await team.getMembers();

    // remove all team references
    const [num, _] = await Player.update(
        { // remove all references to team
            teamId: null,
            ownedTeamId: null,
            role: DbRole.Player
        },
        who);

    // reset their roles
    // console.log(players);
    for (const player of players) {
        const member = guild.members.find(x => x.id == player.discordId);
        if (!member) continue;

        // set the properties we updated. no need to update twice though
        player.teamId = null;
        player.ownedTeamId = null;
        player.role = DbRole.Player;

        if (player.hasPlayerProfile) {
            await clearAndReassignMemberRoles(player, member);
        } else {
            await endPlayer(player, guild);
        }
    }

    // destroy achievements and team
    await TeamAchievements.destroy({
        where: {
            teamId: team.id
        }
    });

    return team.destroy();
}

export async function disbandOrganization(org: Organization, guild: Discord.Guild) {

    const who = {
        where: {
            [Sequelize.Op.or]: {
                organizationId: org.id,
                ownedOrganizationId: org.id
            }
        }
    };

    const players = await Player.findAll(who);

    // remove players from org
    const [num, _] = await Player.update({
        organizationId: null,
        ownedOrganizationId: null,
        role: DbRole.Player
    }, who);

    // reset player roles
    for (const player of players) {
        const member = guild.members.find(x => x.id == player.discordId);
        if (!member) continue;

        // update from the update clause
        player.organizationId = null;
        player.ownedOrganizationId = null;
        player.role = DbRole.Player;

        if (player.hasPlayerProfile || player.hasTeam) {
            await clearAndReassignMemberRoles(player, member);
        } else {
            await endPlayer(player, guild);
        }
    }

    // destroy key points and org
    await KeyPoints.destroy({
        where: {
            organizationId: org.id
        }
    });

    // remove role
    const orgRole = guild.roles.find(x => x.id === org.roleSnowflake);
    if (orgRole) {
        await orgRole.delete();
    }

    return org.destroy();
}
