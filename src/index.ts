import { Sequelize } from "sequelize-typescript";
import Discord from "discord.js";
import { FngBot } from "./bot";
import { ApplicationPoint } from "./model/ApplicationPoint";
import { Event } from "./model/Event";
import { KeyPoints } from "./model/KeyPoints";
import { Organization } from "./model/Organization";
import { Player } from "./model/Player";
import { PlayerAchievement } from "./model/PlayerAchievement";
import { Team } from "./model/Team";
import { TeamAchievements } from "./model/TeamAchievements";
import { UserApplication } from "./model/UserApplication";
import { CommandPermission } from "./model/CommandPermissions";
const config = require("../config.json");

/*process.on("unhandledRejection", (reason, p) => {
    console.log(p);
    console.log(reason);
    console.log(reason.stack);
})*/

console.info("Creating database connection...");
const sequelize = new Sequelize({
    url: config.dburi,
    logging: config.dblog
});

sequelize.addModels([
    ApplicationPoint,
    Event,
    KeyPoints,
    Organization,
    Player,
    PlayerAchievement,
    Team,
    TeamAchievements,
    UserApplication,
    CommandPermission
]);

console.info("Syncing model...");
sequelize.sync();
console.info("Ok!");

console.info("Logging in to discord...")
const botclient = new Discord.Client();
botclient.login(config.token).then(() => {
    const bot = new FngBot(botclient, config);
}).catch(r => {
    console.log("Bot broke! " + r);
});

