import { ChannelContext } from "./channelcontext";
import Discord from 'discord.js';
import { PromptTransitionList, Prompt, PromptAction } from "./prompts";
import { FngBot } from "./bot";

export class StatefulChannelContext extends ChannelContext {

    promptStates: Prompt[];
    private promptTransitions: PromptTransitionList;
    private currentState = -1;

    private responseTimer: NodeJS.Timer = null;

    private ubot: FngBot;

    get bot() {
        return this.ubot;
    }

    get state() {
        return this.promptTransitions[this.currentState];
    }

    protected buildTransitions() {
        this.promptTransitions = {};
        for (const prompt of this.promptStates) {
            this.promptTransitions[prompt.id] = prompt;
        }
    }

    onAccept: () => void = null;

    protected clearChannel(...args: any[]) {
        if (this.bot)
            this.bot.removeContext(this);
            
        this.channel.delete();
        this.channel = null;
    }

    private performState(index: number) {
        const state = this.promptTransitions[index];
        if (!state) {
            console.log(`No state ${index}.`);
            return;
        }

        if (this.responseTimer) {
            clearTimeout(this.responseTimer);
            this.responseTimer = null;
        }

        if (state.accept) {
            
            if (this.onAccept)
                this.onAccept();

            this.clearChannel();
            return;
        }

        // pure state? then we're out
        if (!(state.onReact || state.onResponse)) {
            if (state.action)
                state.action();

            this.currentState = state.getNextPrompt();
            this.performState(this.currentState);
            return;
        }

        this.responseTimer = setTimeout(() => this.clearChannel(), 120000);

        let promise;
        if (state.embed) {
            promise = state.embed().then(
                embed => {
                      return this.channel.send(state.getMessage(), {
                          embed: embed
                      });
                }
            );
        } else {
            promise = this.channel.send(state.getMessage());
        }

        promise.then(msg => this.addReactions(msg, state))
               .then(msg => this.addReactionHook(msg, state));
    }

    private async addReactions(msg: Discord.Message | Discord.Message[], state: Prompt) {
        let m = <Discord.Message>msg;
        if (state.reactions) {
            // sequence the promise
            for (const reaction of state.reactions) {
                var emoji = m.guild.emojis.find(x => x.name === reaction);

                if (!emoji) {
                    emoji = m.client.emojis.find(x => x.name === reaction);
                }

                if (!emoji)
                    console.log(`Missing emoji ${reaction}. It must be a custom emoji for this to work!`);

                await m.react(emoji);
            }
        }
        return m;
    }

    private addReactionHook(msg: Discord.Message, state: Prompt) {
        if (state.reactions && state.onReact) {
            const reactSet = new Set(state.reactions);
            // only allow the prompt's emoji, and don't trigger yourself
            const allowedEmojiAndCorrectUser = (reaction: Discord.MessageReaction, member: Discord.GuildMember) => {
                return reactSet.has(reaction.emoji.name) &&
                    (member.id == this.member.id);
            };
            const collector = msg.createReactionCollector(allowedEmojiAndCorrectUser, {
                time: 120000,
                max: 1
            });
            collector.on("collect", mr => {
                // console.log(mr);
                this.handleStateResult(state.onReact(mr));
            });
        }
    }

    handleStateResult(result: PromptAction) {
        switch (result) {
            case PromptAction.Nothing:
                break;
            case PromptAction.Advance:
                this.currentState = this.state.getNextPrompt();
                this.performState(this.currentState);
                break;
            case PromptAction.Repeat:
                this.performState(this.state.id);
                break;
        }
    }

    // sets state to 0 and activates it
    start(bot: FngBot, startState: number = 0) {
        this.ubot = bot;
        this.currentState = startState;
        this.performState(this.currentState);
    }

    respond(msg: Discord.Message): void {
        // console.log("responding to message");

        if (this.state) {
            if (this.state.onResponse) {
                this.handleStateResult(this.state.onResponse(msg.content));
            }
        } else {
            console.log(`no active state for channel (state = ${this.currentState})`);
        }
    }

}