Announcing...
**{{event.name}}**
```{{event.description}}```

**TIME**: {{event.dateString}}

_More Info_: {{event.link}}
{{#if event.discord}} {{event.discord}} {{/if}}